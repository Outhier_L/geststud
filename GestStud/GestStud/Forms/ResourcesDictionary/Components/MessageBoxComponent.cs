﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GestStud.Forms.ResourcesDictionary.Components
{
    public static class MessageBoxComponent
    {
        public static MessageBoxResult deleteMessageBox(string message)
        {
            return MessageBox.Show("Etes vous sur de vouloir supprimer " + message + " ?", "Confirmation de suppression", MessageBoxButton.YesNo, MessageBoxImage.Stop);
        }

        public static MessageBoxResult updateMessageBox(string message)
        {
            return MessageBox.Show("Nouvelle version de l'application disponible " + message + " mettre à jour ?", "Nouvelle version disponible", MessageBoxButton.YesNo, MessageBoxImage.Information);
        }
    }
}
