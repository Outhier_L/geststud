﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Settings
{
    /// <summary>
    /// Logique d'interaction pour UpdateSetting.xaml
    /// </summary>
    /// 
    public partial class UpdateSetting : Page
    {
        private SettingValidator validator;

        public UpdateSetting(Setting setting)
        {
            InitializeComponent();

            DataContext = setting;

            validator = new SettingValidator();

            txt_updateSetting_title.Text = "Modifier le paramètre " + setting.name;
        }

        private void updateSetting(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Setting)DataContext);

            if (result.IsValid)
            {
                SettingDao.update((Setting)DataContext);

                NavigationService.Navigate(new showSetting());
            }

            Mouse.OverrideCursor = null;

        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
