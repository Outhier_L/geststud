﻿using GestStud.Dao;
using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Settings
{
    /// <summary>
    /// Logique d'interaction pour showSetting.xaml
    /// </summary>
    public partial class showSetting : Page
    {
        private List<Setting> settings;

        public showSetting()
        {
            InitializeComponent();

            settings = completeDataGrid();
        }

        private void goToUpdateViewSetting(object sender, RoutedEventArgs e)
        {
            Setting setting = ((FrameworkElement)sender).DataContext as Setting;

            NavigationService.Navigate(new UpdateSetting(setting));
        }

        private List<Setting> completeDataGrid()
        {
            List<Setting> list = SettingDao.getAll();

            dataGrid_showSetting_listeSettings.ItemsSource = list;

            return list;
        }

        private void input_showSetting_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Setting> finalList = new List<Setting>();

            string searchText = input_showSetting_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_showSetting_deleteSearchBox.Visibility = Visibility.Visible;

                foreach (Setting s in settings)
                {
                    if (s.name.ToLower().Contains(searchText) || s.code.ToString().ToLower().Contains(searchText) || s.description.ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }
                }

                dataGrid_showSetting_listeSettings.ItemsSource = finalList;
            }
            else
            {
                btn_showSetting_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_showSetting_listeSettings.ItemsSource = settings;
            }
        }
        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void clearSearchInput()
        {
            input_showSetting_searchBox.Clear();

            btn_showSetting_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
