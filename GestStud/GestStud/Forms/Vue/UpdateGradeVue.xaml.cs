﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Vue
{
    /// <summary>
    /// Logique d'interaction pour UpdateGradeVue.xaml
    /// </summary>
    public partial class UpdateGradeVue : Page
    {
        private Grade g;
        private Promotion p;
        private GradeValidator validator;
        public UpdateGradeVue(Grade grade, Promotion promotion)
        {
            InitializeComponent();

            DataContext = g = grade;

            validator = new GradeValidator();

            p = promotion;

            txt_updateGradeVue_title.Text = "Modifier la note " + grade.description + " de l'étudiant " + grade.student.first_name + " " + grade.student.last_name;
        }

        private void updateGrade(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Grade)DataContext);

            if (result.IsValid)
            {
                GradeDao.update((Grade)DataContext);

                NavigationService.Navigate(new GradesSubjectsVue(g.subject, p));
            }

            Mouse.OverrideCursor = null;

        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

    }
}
