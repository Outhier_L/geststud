﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Vue
{
    /// <summary>
    /// Logique d'interaction pour GradesSubjectsVue.xaml
    /// </summary>
    public partial class GradesSubjectsVue : Page
    {

        private List<Grade> grades;
        private Promotion p;
        private Subject s;

        public GradesSubjectsVue(Subject subject, Promotion promotion)
        {
            InitializeComponent();

            txt_gradeSubjectVue_title.Text = "Note de la matière " + subject.name;

            grades = GradeDao.getGradeFromPromotionFromSubject(promotion, subject);

            s = subject;

            p = promotion;

            gradeSubjectDataGrid.ItemsSource = grades;

        }

        private void input_gradeSubjectVue_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Grade> finalList = new List<Grade>();

            string searchText = input_gradeSubjectVue_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_gradeSubjectVue_deleteSearchBox.Visibility = Visibility.Visible;

                foreach (Grade g  in grades)
                {
                    if (g.description.ToLower().Contains(searchText) || g.student.first_name.ToLower().Contains(searchText) || g.student.last_name.ToLower().Contains(searchText))
                    {
                        finalList.Add(g);
                    }
                }

                gradeSubjectDataGrid.ItemsSource = finalList;
            }
            else
            {
                btn_gradeSubjectVue_deleteSearchBox.Visibility = Visibility.Hidden;

                gradeSubjectDataGrid.ItemsSource = grades;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void goToEditGradeVue(object sender, RoutedEventArgs e)
        {
            Grade grade = ((FrameworkElement)sender).DataContext as Grade;

            NavigationService.Navigate(new UpdateGradeVue(grade, p));
        }

        private void deleteGrade(object sender, RoutedEventArgs e)
        {
            Grade grade = ((FrameworkElement)sender).DataContext as Grade;

            if (MessageBoxComponent.deleteMessageBox("la note " + grade.description + " de la promotion ") == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                foreach (Grade g in grades)
                {
                    if (g.description == grade.description)
                    {
                        GradeDao.delete(g);
                    }
                }
            }

            NavigationService.Navigate(new GradesSubjectsVue(SubjectDao.getById(s.id), PromotionDao.getById(p.id)));

            Mouse.OverrideCursor = null;
        }

        private void clearSearchInput()
        {
            input_gradeSubjectVue_searchBox.Clear();

            btn_gradeSubjectVue_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
