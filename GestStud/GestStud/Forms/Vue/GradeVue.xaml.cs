﻿using FluentValidation.Results;
using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Type = GestStud.Entities.Type;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Vue
{
    /// <summary>
    /// Logique d'interaction pour GradeVue.xaml
    /// </summary>
    public partial class GradeVue : Page
    {
        private Promotion promo;

        private Subject sub;

        private GradeValidator validator;

        public GradeVue(Subject subject, Promotion promotion)
        {
            InitializeComponent();

            txt_gradeVue_title.Text = "Ajouter une note pour la matière " + subject.name + " pour la promotion " + promotion.name + " " + promotion.years;

            promo = promotion;

            sub = subject;

            validator = new GradeValidator();

            complete();
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void calculMoyenne(object sender, RoutedEventArgs e)
        {
            try
            {
                float moyenne = 0;
                int count = 0;

                for (int i = 0; i < list_gradeVue_students.Items.Count; i++)
                {
                    ListBoxItem lbi = list_gradeVue_students.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

                    TextBox textBox = FindVisualChild<TextBox>(lbi, "input_gradeVue_grade");

                    if(textBox.Text != null)
                    {
                        moyenne = moyenne + float.Parse(textBox.Text);
                        count++;
                    }
                }
                moyenne = moyenne / count;
                txt_gradeVue_average.Text = "Moyenne de la classe : " + moyenne;
            }
            catch(Exception exc)
            {
            }

        }

        private void createGrades(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            try
            {
                for (int i = 0; i < list_gradeVue_students.Items.Count; i++)
                {
                    ListBoxItem lbi = list_gradeVue_students.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

                    TextBox textBox = FindVisualChild<TextBox>(lbi, "input_gradeVue_grade");

                    Grade grade = new Grade();
                    grade.value = float.Parse(textBox.Text);
                    grade.description = input_gradeVue_description.Text;
                    grade.report = (Reports)cmb_gradeVue_report.SelectedItem;
                    grade.type = (Type)cmb_gradeVue_type.SelectedItem;
                    grade.subject = sub;
                    grade.student = (Student)list_gradeVue_students.Items[i];

                    ValidationResult result = validator.Validate(grade);

                    List<Grade> gradesSubject = GradeDao.getGradeListFromSubjectReport(grade.subject);

                    int count = 0;

                    foreach (Grade gradeSubject in gradesSubject)
                    {
                        if (gradeSubject.student.id == ((Student)list_gradeVue_students.Items[i]).id && gradeSubject.description == input_gradeVue_description.Text)
                        {
                            Console.WriteLine("Deja existant");
                            count++;
                        }
                    }
                    if(count == 0)
                    {
                        if (result.IsValid)
                        {
                            GradeDao.create(grade);
                            txt_gradeVue_error.Text = "";
                        }
                        else
                        {
                            txt_gradeVue_error.Text = "Vérifier les champs";
                        }
                    }
                    else
                    {
                        txt_gradeVue_error.Text = "Descritpion déjà existante !";
                    }
                }

                if(txt_gradeVue_error.Text == "")
                {
                    promo = PromotionDao.getById(promo.id);

                    NavigationService.Navigate(new PromotionVue(promo));
                }
            }
            catch(Exception exc)
            {
                txt_gradeVue_error.Text = "Vérifier les champs";
            }


            Mouse.OverrideCursor = null;
        }

        private void complete()
        {
            list_gradeVue_students.ItemsSource = promo.students;

            cmb_gradeVue_report.ItemsSource = Enum.GetValues(typeof(Reports));

            cmb_gradeVue_report.SelectedIndex = 0;

            cmb_gradeVue_type.ItemsSource = TypeDao.getAll();

            cmb_gradeVue_type.SelectedIndex = 0;
        }

        private T FindVisualChild<T>(DependencyObject parent, string childName) where T : DependencyObject
        {
            if (parent == null)
                return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                T childType = child as T;
                if (childType == null)
                {
                    foundChild = FindVisualChild<T>(child, childName);

                    if (foundChild != null)
                        break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    FrameworkElement frameworkElement = child as FrameworkElement;
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }
    }
}
