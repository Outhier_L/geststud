﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Calculation;
using GestStud.Tools.Exports;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Vue
{
    /// <summary>
    /// Logique d'interaction pour ReportVue.xaml
    /// </summary>
    public partial class ReportVue : Page
    {
        private Student s;

        private Reports r;

        private double o;

        private List<Grade> g;

        private List<ReportSubjectGroups> reportSubjectGroups;

        private List<ReportGrade> reportGrades;

        private List<ReportSubject> reportSubjects;


        public ReportVue(Student student, Reports report, double overall, List<Grade> grades)
        {
            InitializeComponent();

            s = student;

            r = report;

            o = overall;

            g = grades;

            txt_reportVue_average.Text = "Moyenne générale : " + s.overallAverage;
            txt_reportVue_infoAverage.Text = "Moyenne informatique : " + s.infoOverallAverage;

            txt_reportVue_promoAverage.Text = "Moyenne de la classe : " + o;

            txt_reportVue_title.Text = "Bulletins de " + r + " pour "  + s.first_name + " " + s.last_name;
            
            reportSubjectGroups = new List<ReportSubjectGroups>();
            reportGrades = new List<ReportGrade>();
            reportSubjects = new List<ReportSubject>();

            foreach(Grade grade in g)
            {
                //Partie notes
                ReportGrade reportGrade = new ReportGrade(grade, student.promotion);
                reportGrades.Add(reportGrade);

                //Partie matière
                ReportSubject reportSubject = new ReportSubject(grade);
                int countSubject = 0;

                foreach (ReportSubject rs in reportSubjects)
                {
                    if (rs.name == reportSubject.name)
                    {
                        countSubject++;
                    }
                }
                if (countSubject == 0)
                {
                    reportSubjects.Add(reportSubject);
                }
            }

            //Partie Groupe de matière
            foreach (ReportSubject reportSubject in reportSubjects)
            {
                ReportSubjectGroups reportSubjectGroup = new ReportSubjectGroups(reportSubjects, reportSubject);
                Console.WriteLine(reportSubjectGroup.name);
                
                int count = 0;
                if (reportSubjectGroup.name != "Sans groupe")
                {
                    foreach (ReportSubjectGroups rsg in reportSubjectGroups)
                    {
                        if (reportSubjectGroup.name == rsg.name)
                            count++;
                    }
                    if (count == 0)
                        reportSubjectGroups.Add(reportSubjectGroup);
                }
            }
            
            gradeDataGrid.ItemsSource = reportGrades;
            subjectGroupDataGrid.ItemsSource = reportSubjectGroups;
            subjectDataGrid.ItemsSource = reportSubjects;
        }

        private void exportReportInExcel(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            if (r.Equals(Reports.Certification))
            {
                var workbook = ExportReportToExcel.buildCertifExcel(s, g);

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier Excel |*.xlsx";
                saveFileDialog.Title = "Enregistrer le bulletin " + r + " de " + s.first_name + " " + s.last_name + " en XLSX";

                if (saveFileDialog.ShowDialog() == true)
                {
                    workbook.SaveAs(saveFileDialog.FileName);
                    ExportReportToExcel.closeExcel();
                }
            }
            else
            {
                var workbook = ExportReportToExcel.buildExcelReport(s, g, r, o);

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier Excel |*.xlsx";
                saveFileDialog.Title = "Enregistrer le bulletin " + r + " de " + s.first_name + " " + s.last_name + " en XLSX";

                if (saveFileDialog.ShowDialog() == true)
                {
                    workbook.SaveAs(saveFileDialog.FileName);
                    ExportReportToExcel.closeExcel();
                }
            }

            Mouse.OverrideCursor = null;
        }

        private void exportReportInPDF(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            if(r.Equals(Reports.Certification))
            {
                var workbook = ExportReportToExcel.buildCertifExcel(s, g);

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier PDF|*.pdf";
                saveFileDialog.Title = "Enregistrer le bulletin " + r + " de " + s.first_name + " " + s.last_name + " en PDF";

                if (saveFileDialog.ShowDialog() == true)
                {
                    workbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, saveFileDialog.FileName);
                    ExportReportToExcel.closeSheet();
                    ExportReportToExcel.closeExcel();
                }
            }
            else
            {
                var workbook = ExportReportToExcel.buildExcelReport(s, g, r, o);

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier PDF|*.pdf";
                saveFileDialog.Title = "Enregistrer le bulletin " + r + " de " + s.first_name + " " + s.last_name + " en PDF";

                if (saveFileDialog.ShowDialog() == true)
                {
                    workbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, saveFileDialog.FileName);
                    ExportReportToExcel.closeSheet();
                    ExportReportToExcel.closeExcel();
                }
            }

            Mouse.OverrideCursor = null;
        }
    }
}
