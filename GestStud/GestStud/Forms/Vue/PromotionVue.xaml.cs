﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.Promotions;
using GestStud.Forms.ResourcesDictionary.Components;
using GestStud.Forms.Students;
using GestStud.Tools.Calculation;
using GestStud.Tools.Exports;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Vue
{
    /// <summary>
    /// Logique d'interaction pour PromotionVue.xaml
    /// </summary>
    public partial class PromotionVue : Page
    {

        private Promotion promo;

        private double o;
        public PromotionVue(Promotion promotion)
        {
            InitializeComponent();

            if (promotion != null)
            {
                lbl_promotionVue_title.Text = "Promotion " + promotion.name + " " + promotion.years;

                promo = promotion;

                completeComboBox();

                completeDataGrid();
            }

        }

        private void goToReportVue(object sender, RoutedEventArgs e)
        {
            Student student = ((FrameworkElement)sender).DataContext as Student;

            Reports report = (Reports)input_promotionVue_reports.SelectedItem;

            NavigationService.Navigate(new ReportVue(student, report, o, GradeDao.getGradeListFormReport(report, student.grades)));
        }

        private void goToCreateStudent(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreateStudent());
        }

        private void goToAddSubject(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AddSubjects(promo));
        }

        private void goToGradeVue(object sender, RoutedEventArgs e)
        {
            Subject subject = ((FrameworkElement)sender).DataContext as Subject;

            NavigationService.Navigate(new GradeVue(subject, promo));
        }
        private void deleteSubject(object sender, RoutedEventArgs e)
        {
            Subject subject = ((FrameworkElement)sender).DataContext as Subject;

            if (MessageBoxComponent.deleteMessageBox("la matière " + subject.name + " dans la promotion " + promo.name + " " + promo.years) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                promo = PromotionDao.removeSubjectsInPromotion(promo, subject);

                completeDataGrid();

                studentDataGrid.ItemsSource = getStudentsList();

                clearSearchBox();
            }

            Mouse.OverrideCursor = null;
        }

        private void completeDataGrid()
        {
            subjectDataGrid.ItemsSource = promo.subjects;
        }

        private void completeComboBox()
        {
            input_promotionVue_reports.ItemsSource = Enum.GetValues(typeof(Reports));

            input_promotionVue_reports.SelectedIndex = 0;
        }

        private void input_promotionVue_searchBoxStudent_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Student> finalList = new List<Student>();

            string searchText = input_promotionVue_searchBoxStudent.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_promotionVue_deleteSearchBoxStudent.Visibility = Visibility.Visible;

                foreach (Student s in promo.students)
                {
                    if (s.first_name.ToLower().Contains(searchText) || s.last_name.ToString().ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }
                }

                studentDataGrid.ItemsSource = finalList;
            }
            else
            {
                btn_promotionVue_deleteSearchBoxStudent.Visibility = Visibility.Hidden;

                studentDataGrid.ItemsSource = promo.students;
            }
        }

        private void deleteStudentSearchBox(object sender, RoutedEventArgs e)
        {
            input_promotionVue_searchBoxStudent.Clear();

            btn_promotionVue_deleteSearchBoxStudent.Visibility = Visibility.Hidden;
        }

        private void input_promotionVue_searchBoxSubject_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Subject> finalList = new List<Subject>();

            string searchText = input_promotionVue_searchBoxSubject.Text.ToLower();

            if (!searchText.Equals(""))
            {
                foreach (Subject s in promo.subjects)
                {
                    btn_promotionVue_deleteSearchBoxSubject.Visibility = Visibility.Visible;

                    if (s.name.ToLower().Contains(searchText) || s.coefficient.ToString().ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }

                    if (s.subjectGroup != null)
                    {
                        if (s.subjectGroup.name.ToLower().Contains(searchText) && !finalList.Contains(s))
                        {
                            finalList.Add(s);
                        }
                    }
                }

                subjectDataGrid.ItemsSource = finalList;
            }
            else
            {
                btn_promotionVue_deleteSearchBoxSubject.Visibility = Visibility.Hidden;

                subjectDataGrid.ItemsSource = promo.subjects;
            }
        }

        private void deleteSubjectSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchBox();
        }

        private void clearSearchBox()
        {
            input_promotionVue_searchBoxSubject.Clear();

            btn_promotionVue_deleteSearchBoxSubject.Visibility = Visibility.Hidden;
        }


        private List<Student> getStudentsList()
        {
            List<Student> students = new List<Student>();

            Reports report = (Reports)input_promotionVue_reports.SelectedItem;

            foreach (Student s in promo.students)
            {
                Student student;

                student = CalculationOverallGrade.getGeneralOverallGradeOfStudent(s, report);
                student = CalculationOverallGrade.getInfoOverallGradeOfStudent(student, report);

                students.Add(student);
            }
            o = CalculationOverallGrade.getOverallGradeFromPromotion(students);
            txt_promotionVue_average.Text = "Moyenne de la classe : " + o;

            return students;
        }

        private void input_promotionVue_reports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            studentDataGrid.ItemsSource = getStudentsList();
        }

        private void goToGradesSubjectsVue(object sender, RoutedEventArgs e)
        {
            Subject subject = ((FrameworkElement)sender).DataContext as Subject;

            NavigationService.Navigate(new GradesSubjectsVue(subject, promo));
        }

        private void exportReportInExcel(object sender, RoutedEventArgs e)
        {
            Reports report = (Reports)input_promotionVue_reports.SelectedItem;

            if(report.Equals(Reports.Certification))
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier Excel |*.xlsx";
                saveFileDialog.Title = "Enregistrer les bulletins en XLSX";
                saveFileDialog.FileName = "Bulletin de ";

                if (saveFileDialog.ShowDialog() == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    foreach (Student s in promo.students)
                    {
                        var workbook = ExportReportToExcel.buildCertifExcel(s, GradeDao.getGradeListFormReport(report, s.grades));
                        workbook.SaveAs(saveFileDialog.FileName.Split('.')[0] + s.last_name);
                        ExportReportToExcel.closeExcel();
                    }

                }
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier Excel |*.xlsx";
                saveFileDialog.Title = "Enregistrer les bulletins en XLSX";
                saveFileDialog.FileName = "Bulletin de ";

                if (saveFileDialog.ShowDialog() == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    foreach (Student s in promo.students)
                    {
                        var workbook = ExportReportToExcel.buildExcelReport(s, GradeDao.getGradeListFormReport(report, s.grades), report, o);
                        workbook.SaveAs(saveFileDialog.FileName.Split('.')[0] + s.last_name);
                        ExportReportToExcel.closeExcel();
                    }

                }
            }
           

            Mouse.OverrideCursor = null;
        }

        private void exportReportInPDF(object sender, RoutedEventArgs e)
        {
            Reports report = (Reports)input_promotionVue_reports.SelectedItem;

            if(report.Equals(Reports.Certification))
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier PDF |*.pdf";
                saveFileDialog.Title = "Enregistrer les bulletins en PDF";
                saveFileDialog.FileName = "Bulletin de ";

                if (saveFileDialog.ShowDialog() == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    foreach (Student s in promo.students)
                    {

                        var workbook = ExportReportToExcel.buildCertifExcel(s, GradeDao.getGradeListFormReport(report, s.grades));
                        workbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, saveFileDialog.FileName.Split('.')[0] + s.last_name);
                        ExportReportToExcel.closeSheet();
                        ExportReportToExcel.closeExcel();
                    }

                }
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Fichier PDF |*.pdf";
                saveFileDialog.Title = "Enregistrer les bulletins en PDF";
                saveFileDialog.FileName = "Bulletin de ";

                if (saveFileDialog.ShowDialog() == true)
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    foreach (Student s in promo.students)
                    {

                        var workbook = ExportReportToExcel.buildExcelReport(s, GradeDao.getGradeListFormReport(report, s.grades), report, o);
                        workbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, saveFileDialog.FileName.Split('.')[0] + s.last_name);
                        ExportReportToExcel.closeSheet();
                        ExportReportToExcel.closeExcel();
                    }

                }
            }
           

            Mouse.OverrideCursor = null;
        }

    }
}
