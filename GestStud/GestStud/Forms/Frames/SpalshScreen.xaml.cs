﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GestStud.Forms.Frames
{
    /// <summary>
    /// Logique d'interaction pour SpalshScreen.xaml
    /// </summary>
    public partial class SpalshScreen : Window
    {
        public bool CanClose { get; set; } = false;
        public SpalshScreen()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !CanClose;
        }

        public void initializeDownloadScreen()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Title = "Mise à jour de l'application Geststud";

                txt_splashScreen_text.Text = "Veuillez patientez lors de la mise à jour de l'application ...";
                txt_splashScreen_text.Visibility = Visibility.Visible;

                pgr_SplashScreen_progress.IsIndeterminate = false;

            });
        }

        public void SetText(string text)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                txt_splashScreen_load.Text = text;
            });
        }

        public void UpdateProgress(int i)
        {
            Console.WriteLine(i);
            Application.Current.Dispatcher.Invoke(() => this.pgr_SplashScreen_progress.Value = i);
        }
    }
}
