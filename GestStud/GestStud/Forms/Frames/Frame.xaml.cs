﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.Promotions;
using GestStud.Forms.Settings;
using GestStud.Forms.Students;
using GestStud.Forms.Subjects;
using GestStud.Forms.SubjectsGroups;
using GestStud.Forms.Types;
using GestStud.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms
{
    /// <summary>
    /// Logique d'interaction pour Frame.xaml
    /// </summary>
    public partial class Frame : Window
    {
        public Frame()
        {
            InitializeComponent();

            Title += Assembly.GetEntryAssembly().GetName().Version;

            MainFrame.NavigationService.Navigate(new HomePage());
        }
        
        private void goBack(object sender, RoutedEventArgs e)
        {
            if(MainFrame.NavigationService.CanGoBack)
            {
                MainFrame.NavigationService.GoBack();
            }
        }

        private void MainFrame_Navigated(object sender, NavigationEventArgs e)
        {
            if (MainFrame.NavigationService.CanGoBack)
            {
                btn_mainFrame_return.Visibility = Visibility.Visible;
            }
            else
            {
                btn_mainFrame_return.Visibility = Visibility.Hidden;
            }
        }

        private void exitApp(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void goToHome(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new HomePage());
        }

        private void goToShowType(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new ShowType());
        }

        private void goToCreateType(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new CreateType());
        }

        private void goToCreatePromotion(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new CreatePromotion());
        }

        private void goToShowPromotion(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new ShowPromotion());
        }

        private void goToCreateSubjectGroup(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new CreateSubjectGroup());
        }

        private void goToShowSubjectGroup(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new ShowSubjectGroup());
        }
        private void goToCreateSubject(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new CreateSubject());
        }

        private void goToShowSubject(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new ShowSubject());
        }

        private void goToCreateStudent(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new CreateStudent());
        }

        private void goToShowStudent(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new ShowStudent());
        }
        private void goToShowSetting(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new showSetting());
        }
    }
}
