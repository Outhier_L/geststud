﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Type = GestStud.Entities.Type;
using FluentValidation.Results;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Types
{
    /// <summary>
    /// Logique d'interaction pour UpdateType.xaml
    /// </summary>
    public partial class UpdateType : Page
    {
        private readonly TypeValidator validator;
        public UpdateType(Type type)
        {
            InitializeComponent();

            DataContext = type;

            validator = new TypeValidator();

            txt_updateType_title.Text = "Mise à jour du type de notation " + type.name;
        }

        private void updateType(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Type)DataContext);

            if (result.IsValid)
            {
                TypeDao.update((Type)DataContext);

                NavigationService.Navigate(new ShowType());
            }

            Mouse.OverrideCursor = null;

        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
