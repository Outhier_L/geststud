﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Type = GestStud.Entities.Type;

namespace GestStud.Forms.Types
{
    /// <summary>
    /// Logique d'interaction pour ShowType.xaml
    /// </summary>
    public partial class ShowType : Page
    {

        private List<Type> list;
        public ShowType()
        {
            InitializeComponent();

            list = TypeDao.getAll();

            dataGrid_showType_listeType.ItemsSource = list;
        }

        private void goToUpdateViewType(object sender, RoutedEventArgs e)
        {
            Type type = ((FrameworkElement)sender).DataContext as Type;

            NavigationService.Navigate(new UpdateType(type));
        }
        private void deleteType(object sender, RoutedEventArgs e)
        {
            Type type = ((FrameworkElement)sender).DataContext as Type;
          
            if (MessageBoxComponent.deleteMessageBox("le type '" + type.name) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                TypeDao.delete(type);

                list = TypeDao.getAll();

                dataGrid_showType_listeType.ItemsSource = list;

                clearSearchBox();
            }

            Mouse.OverrideCursor = null;
        }

        private void goToCreateViewType(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreateType());
        }

        private void input_showType_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Type> finalList = new List<Type>();

            string searchText = input_showType_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_showType_deleteSearchBox.Visibility = Visibility.Visible;

                foreach(Type t in list)
                {
                    if (t.name.ToLower().Contains(searchText) || t.coefficient.ToString().ToLower().Contains(searchText))
                    {
                        finalList.Add(t);
                    }
                }

                dataGrid_showType_listeType.ItemsSource = finalList;
            }
            else
            {
                btn_showType_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_showType_listeType.ItemsSource = list;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchBox();
        }

        private void clearSearchBox()
        {
            input_showType_searchBox.Clear();

            btn_showType_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
