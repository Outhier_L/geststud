﻿using FluentValidation.Results;
using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Students
{
    /// <summary>
    /// Logique d'interaction pour UpdateStudent.xaml
    /// </summary>
    public partial class UpdateStudent : Page
    {
        private StudentValidator validator;
        public UpdateStudent(Student student)
        {
            InitializeComponent();

            DataContext = student;

            validator = new StudentValidator();

            txt_updateStudent_title.Text = "Modifier l'étudiant " + student.first_name + " " + student.last_name;

            completeCombo();

        }

        private void updateStudent(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Student)DataContext);

            if (result.IsValid)
            {
                StudentDao.update((Student)DataContext);

                NavigationService.Navigate(new ShowStudent());
            }

            Mouse.OverrideCursor = null;

        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void completeCombo()
        {
            List<Promotion> promotions = PromotionDao.getAll();

            if (promotions.Count == 0)
            {
                txt_createStudent_errorPromo.Visibility = Visibility.Visible;
                input_createStudent_Promotion.Visibility = Visibility.Hidden;
            }
            else
            {
                input_createStudent_Promotion.ItemsSource = promotions;
            }
        }
    }
}
