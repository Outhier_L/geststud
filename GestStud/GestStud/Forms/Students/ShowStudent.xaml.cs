﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Students
{
    /// <summary>
    /// Logique d'interaction pour ShowStudent.xaml
    /// </summary>
    public partial class ShowStudent : Page
    {
        private List<Student> students;
        public ShowStudent()
        {
            InitializeComponent();

            students = completeDataGrid();
        }

        private void goToUpdateViewStudent(object sender, RoutedEventArgs e)
        {
            Student student = ((FrameworkElement)sender).DataContext as Student;

            NavigationService.Navigate(new UpdateStudent(student));
        }
        private void deleteStudent(object sender, RoutedEventArgs e)
        {
            Student student = ((FrameworkElement)sender).DataContext as Student;

            if (MessageBoxComponent.deleteMessageBox("l'étudiant " + student.first_name + " " + student.last_name) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                StudentDao.delete(student);

                students = completeDataGrid();

                clearSearchInput();
            }

            Mouse.OverrideCursor = null;
        }

        private void goToCreateViewStudent(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreateStudent());
        }

        private List<Student> completeDataGrid()
        {
            List<Student> list = StudentDao.getAll();

            dataGrid_showType_listeStudent.ItemsSource = list;

            return list;
        }

        private void input_showStudent_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Student> finalList = new List<Student>();

            string searchText = input_showStudent_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_showStudent_deleteSearchBox.Visibility = Visibility.Visible;

                foreach (Student s in students)
                {
                    if (s.first_name.ToLower().Contains(searchText) || s.last_name.ToString().ToLower().Contains(searchText) || s.promotion.name.ToLower().Contains(searchText) || s.promotion.years.ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }
                }

                dataGrid_showType_listeStudent.ItemsSource = finalList;
            }
            else
            {
                btn_showStudent_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_showType_listeStudent.ItemsSource = students;
            }
        }
        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void clearSearchInput()
        {
            input_showStudent_searchBox.Clear();

            btn_showStudent_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
