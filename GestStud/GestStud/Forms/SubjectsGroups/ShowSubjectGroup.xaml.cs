﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.SubjectsGroups
{
    /// <summary>
    /// Logique d'interaction pour ShowSubjectGroup.xaml
    /// </summary>
    public partial class ShowSubjectGroup : Page
    {
        private List<SubjectGroup> subjectGroups;
        public ShowSubjectGroup()
        {
            InitializeComponent();

            subjectGroups = completeDataGrid();
        }

        private void goToUpdateViewSubjectGroup(object sender, RoutedEventArgs e)
        {
            SubjectGroup subjectGroup = ((FrameworkElement)sender).DataContext as SubjectGroup;

            NavigationService.Navigate(new UpdateSubjectGroup(subjectGroup));
        }
        private void deleteSubjectGroup(object sender, RoutedEventArgs e)
        {
            SubjectGroup subjectGroup = ((FrameworkElement)sender).DataContext as SubjectGroup;

            if (MessageBoxComponent.deleteMessageBox("le groupe de matières " + subjectGroup.name) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                SubjectGroupDao.delete(subjectGroup);

                subjectGroups = completeDataGrid();

                clearSearchInput();
            }

            Mouse.OverrideCursor = null;
        }

        private void goToCreateViewSubjectGroup(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreateSubjectGroup());
        }

        private void goToViewSubjects(object sender, RoutedEventArgs e)
        {
            SubjectGroup subjectGroup = ((FrameworkElement)sender).DataContext as SubjectGroup;

            NavigationService.Navigate(new ViewSubjectsSubjectGroup(subjectGroup));
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private List<SubjectGroup> completeDataGrid()
        {
            List<SubjectGroup> list = SubjectGroupDao.getAll();

            dataGrid_showSubjectGroup_listeSubjectGroup.ItemsSource = list;

            return list;
        }

        private void input_showSubjectGroup_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<SubjectGroup> finalList = new List<SubjectGroup>();

            string searchText = input_showSubjectGroup_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_showSubjectGroup_deleteSearchBox.Visibility = Visibility.Visible;

                foreach (SubjectGroup s in subjectGroups)
                {
                    if (s.name.ToLower().Contains(searchText) || s.coefficient.ToString().ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }
                }

                dataGrid_showSubjectGroup_listeSubjectGroup.ItemsSource = finalList;
            }
            else
            {
                btn_showSubjectGroup_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_showSubjectGroup_listeSubjectGroup.ItemsSource = subjectGroups;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void clearSearchInput()
        {
            input_showSubjectGroup_searchBox.Clear();

            btn_showSubjectGroup_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
