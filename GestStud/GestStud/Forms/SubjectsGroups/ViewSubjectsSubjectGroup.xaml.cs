﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.SubjectsGroups
{
    /// <summary>
    /// Logique d'interaction pour ViewSubjectsSubjectGroup.xaml
    /// </summary>
    public partial class ViewSubjectsSubjectGroup : Page
    {
        private SubjectGroup s; 
        public ViewSubjectsSubjectGroup(SubjectGroup subjectGroup)
        {
            InitializeComponent();

            txt_viewSubjectsSubkectGroup_title.Text = "Liste des matières pour le groupe " + subjectGroup.name;

            dataGrid_viewSubjectsSubjectGroup_listSubject.ItemsSource = subjectGroup.subjects;

            s = subjectGroup;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void goToAddSubjects(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AddSubjectsSubjectGroup(s));
        }

        private void removeSubject(object sender, RoutedEventArgs e)
        {
            Subject subject = ((FrameworkElement)sender).DataContext as Subject;

            if (MessageBoxComponent.deleteMessageBox("la matière " + subject.name + " dans le groupe " + s.name) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                s = SubjectGroupDao.removeSubjectsInSubjectGroup(s, subject);

                dataGrid_viewSubjectsSubjectGroup_listSubject.ItemsSource = s.subjects;

                clearSearchInput();
            }

            Mouse.OverrideCursor = null;
        }

        private void input_viewSubjectGroup_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Subject> finalList = new List<Subject>();

            string searchText = input_viewSubjectGroup_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                foreach (Subject s in s.subjects)
                {
                    btn_viewSubjectGroup_deleteSearchBox.Visibility = Visibility.Visible;

                    if (s.name.ToLower().Contains(searchText) || s.coefficient.ToString().ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }

                    if (s.subjectGroup != null)
                    {
                        if (s.subjectGroup.name.ToLower().Contains(searchText) && !finalList.Contains(s))
                        {
                            finalList.Add(s);
                        }
                    }
                }

                dataGrid_viewSubjectsSubjectGroup_listSubject.ItemsSource = finalList;
            }
            else
            {
                btn_viewSubjectGroup_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_viewSubjectsSubjectGroup_listSubject.ItemsSource = s.subjects;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void clearSearchInput()
        {
            input_viewSubjectGroup_searchBox.Clear();

            btn_viewSubjectGroup_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
