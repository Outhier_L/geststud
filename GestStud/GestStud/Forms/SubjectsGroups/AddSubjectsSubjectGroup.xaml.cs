﻿using GestStud.Dao;
using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.SubjectsGroups
{
    /// <summary>
    /// Logique d'interaction pour AddSubjectsSubjectGroup.xaml
    /// </summary>
    public partial class AddSubjectsSubjectGroup : Page
    {
        public AddSubjectsSubjectGroup(SubjectGroup subjectGroup)
        {
            InitializeComponent();

            txt_addSubjects_title.Text = "Ajouter des matières au groupe " + subjectGroup.name;

            input_addSubjects_subjects.ItemsSource = SubjectDao.getDifferenceBetweenListSubjects(subjectGroup.subjects.ToList());

            DataContext = subjectGroup;
        }

        private void AddSubjects(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            SubjectGroup subjectGroup = (SubjectGroup)DataContext;

            foreach (Subject s in input_addSubjects_subjects.SelectedItems)
            {
                subjectGroup.subjects.Add(s);
            }

            SubjectGroupDao.addSubjectsInSubjectGroup(subjectGroup);

            NavigationService.Navigate(new ViewSubjectsSubjectGroup(subjectGroup));

            Mouse.OverrideCursor = null;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
