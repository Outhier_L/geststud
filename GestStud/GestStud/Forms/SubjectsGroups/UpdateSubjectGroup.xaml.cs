﻿using FluentValidation.Results;
using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.SubjectsGroups
{
    /// <summary>
    /// Logique d'interaction pour UpdateSubjectGroup.xaml
    /// </summary>
    public partial class UpdateSubjectGroup : Page
    {
        private readonly SubjectsGroupValidator validator;

        public UpdateSubjectGroup(SubjectGroup subjectgroup)
        {
            InitializeComponent();

            txt_updateSubjectGroup_title.Text = "Mise à jour du groupe de matière " + subjectgroup.name;

            DataContext = subjectgroup;

            validator = new SubjectsGroupValidator();
           
        }
        private void updateSubjectGroup(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((SubjectGroup)DataContext);

            if(result.IsValid)
            {
                SubjectGroupDao.update((SubjectGroup)DataContext);

                NavigationService.Navigate(new ShowSubjectGroup());
            }

            Mouse.OverrideCursor = null;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
