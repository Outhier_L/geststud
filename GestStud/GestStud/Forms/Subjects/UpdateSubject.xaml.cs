﻿using FluentValidation.Results;
using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Subjects
{
    /// <summary>
    /// Logique d'interaction pour UpdateSubject.xaml
    /// </summary>
    public partial class UpdateSubject : Page
    {
        private readonly SubjectValidator validator;

        public List<SubjectGroup> subjectGroups { get; set; }
        public UpdateSubject(Subject subject)
        {
            InitializeComponent();

            DataContext = subject;

            validator = new SubjectValidator();

            txt_updateSubject_title.Text = "Mise à jour de la matière " + subject.name;

            completeCombo();
        }
        private void updateSubject(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Subject)DataContext);

            if(result.IsValid)
            {
                SubjectDao.update((Subject)DataContext);

                NavigationService.Navigate(new ShowSubject());
            }

            Mouse.OverrideCursor = null;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void completeCombo()
        {
            subjectGroups = SubjectGroupDao.getAll();

            if (subjectGroups.Count == 0)
            {
                txt_updateSubject_errorGroup.Visibility = Visibility.Visible;
                input_updateSubject_SubjectGroup.Visibility = Visibility.Hidden;
            }
            else
            {
               input_updateSubject_SubjectGroup.ItemsSource = subjectGroups;
            }
        }
    }
}
