﻿using FluentValidation.Results;
using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Subjects
{
    /// <summary>
    /// Logique d'interaction pour CreateSubject.xaml
    /// </summary>
    public partial class CreateSubject : Page
    {
        private readonly SubjectValidator validator;
        public CreateSubject()
        {
            InitializeComponent();

            DataContext = new Subject();

            validator = new SubjectValidator();

            completeCombo();
        }

        private void createSubject(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Subject)DataContext);

            if(result.IsValid)
            {
                SubjectDao.create((Subject)DataContext);

                NavigationService.Navigate(new ShowSubject());
            }

            Mouse.OverrideCursor = null;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void completeCombo()
        {
            List<SubjectGroup> subjectGroups = SubjectGroupDao.getAll();

            if (subjectGroups.Count == 0)
            {
                txt_createSubject_errorGroup.Visibility = Visibility.Visible;
                input_createSubject_SubjectGroup.Visibility = Visibility.Hidden;
            }
            else
            {
                input_createSubject_SubjectGroup.ItemsSource = subjectGroups;

                input_createSubject_SubjectGroup.SelectedIndex = 0;
            }
        }
    }
}
