﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Subjects
{
    /// <summary>
    /// Logique d'interaction pour ShowSubject.xaml
    /// </summary>
    public partial class ShowSubject : Page
    {
        private List<Subject> subjects;
        public ShowSubject()
        {
            InitializeComponent();

            subjects = completeDataGrid();
        }

        private void goToUpdateViewSubject(object sender, RoutedEventArgs e)
        {
            Subject subject = ((FrameworkElement)sender).DataContext as Subject;

            NavigationService.Navigate(new UpdateSubject(subject));
        }
        private void deleteSubject(object sender, RoutedEventArgs e)
        {
            Subject subject = ((FrameworkElement)sender).DataContext as Subject;

            if (MessageBoxComponent.deleteMessageBox("la matière " + subject.name) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                SubjectDao.delete(subject);

                subjects = completeDataGrid();

                clearSearchInput();
            }

            Mouse.OverrideCursor = null;
        }

        private void goToCreateViewSubject(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreateSubject());
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private List<Subject> completeDataGrid()
        {
            List<Subject> list = SubjectDao.getAll();

            dataGrid_showSubject_listeSubject.ItemsSource = list;

            return list;
        }

        private void input_showSubjects_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Subject> finalList = new List<Subject>();

            string searchText = input_showSubjects_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                foreach (Subject s in subjects)
                {
                    btn_showSubject_deleteSearchBox.Visibility = Visibility.Visible;

                    if (s.name.ToLower().Contains(searchText) || s.coefficient.ToString().ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }

                    if (s.subjectGroup != null)
                    {
                        if (s.subjectGroup.name.ToLower().Contains(searchText) && !finalList.Contains(s))
                        {
                            finalList.Add(s);
                        }
                    }
                }

                dataGrid_showSubject_listeSubject.ItemsSource = finalList;
            }
            else
            {
                btn_showSubject_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_showSubject_listeSubject.ItemsSource = subjects;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void clearSearchInput()
        {
            input_showSubjects_searchBox.Clear();

            btn_showSubject_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
