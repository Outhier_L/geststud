﻿using GestStud.Entities;
using GestStud.Forms.Students;

﻿using GestStud.Dao;
using GestStud.Forms.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GestStud.Forms.SubjectsGroups;
using GestStud.Forms.Vue;
using GestStud.Forms.Promotions;

namespace GestStud.Forms
{
    /// <summary>
    /// Logique d'interaction pour HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        private List<Promotion> promotions;
        public HomePage()
        {
            InitializeComponent();

            promotions = completeList(false);
        }

        private List<Promotion> completeList(bool oldPromo)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            List<Promotion> list = new List<Promotion>();

            if (oldPromo == true)
            {
                list = PromotionDao.getAll().OrderBy(p => !p.is_active).ToList();
            }
            else
            {
                list = PromotionDao.getActivePromotions();
            }
      

            if (list.Count > 0)
            {
                list_Accueil_promo.ItemsSource = list;
            }
            else
            {
                list_Accueil_promo.Visibility = Visibility.Hidden;
                lbl_error_list.Visibility = Visibility.Visible;
                btn_homepage_createPromotion.Visibility = Visibility.Visible;
                lbl_error_list.Content = "Aucune promotion n'a été créer";
            }

            Mouse.OverrideCursor = null;

            return list;
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            input_home_searchBox.Clear();

            if ((bool) btn_home_oldPromo.IsChecked)
            {
                promotions = completeList(true);
                pnl_home_legends.Visibility = Visibility.Visible;

                IconEye.Icon = FontAwesome.WPF.FontAwesomeIcon.EyeSlash;
            }
            else
            {
                promotions = completeList(false);
                pnl_home_legends.Visibility = Visibility.Hidden;

                IconEye.Icon = FontAwesome.WPF.FontAwesomeIcon.Eye;
            }
        }

        private void goToPromotionVue(object sender, MouseButtonEventArgs e)
        {
            Promotion p = (Promotion)list_Accueil_promo.SelectedItem;

            NavigationService.Navigate(new PromotionVue(p));
        }

        private void goToCreatePromotion(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreatePromotion());
        }

        private void input_home_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Promotion> finalList = new List<Promotion>();

            string searchText = input_home_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_home_deleteSearchBox.Visibility = Visibility.Visible;

                foreach (Promotion p in promotions)
                {
                    if (p.name.ToLower().Contains(searchText) || p.years.ToLower().Contains(searchText))
                    {
                        finalList.Add(p);
                    }
                }

                list_Accueil_promo.ItemsSource = finalList;
            }
            else
            {
                btn_home_deleteSearchBox.Visibility = Visibility.Hidden;

                list_Accueil_promo.ItemsSource = promotions;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            input_home_searchBox.Clear();

            btn_home_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
