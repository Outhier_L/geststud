﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FluentValidation;
using FluentValidation.Results;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Promotions
{
    /// <summary>
    /// Logique d'interaction pour CreatePromotion.xaml
    /// </summary>
    public partial class CreatePromotion : Page
    {
        private readonly PromotionValidator validator;
        public CreatePromotion()
        {
            InitializeComponent();

            DataContext = new Promotion();

            validator = new PromotionValidator();
        }

        private void createPromotion(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Promotion)DataContext);

            if(result.IsValid)
            {
                PromotionDao.create((Promotion)DataContext);

                NavigationService.Navigate(new ShowPromotion());
            }

            Mouse.OverrideCursor = null;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
