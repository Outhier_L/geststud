﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Promotions
{
    /// <summary>
    /// Logique d'interaction pour ShowPromotion.xaml
    /// </summary>
    public partial class ShowPromotion : Page
    {
        private List<Promotion> promotions;
        public ShowPromotion()
        {
            InitializeComponent();

            promotions = completeTable();
        }

        private void goToUpdatePromotion(object sender, RoutedEventArgs e)
        {
            Promotion promotion = ((FrameworkElement)sender).DataContext as Promotion;

            NavigationService.Navigate(new UpdatePromotion(promotion));
        }
        private void deletePromotion(object sender, RoutedEventArgs e)
        {
            Promotion promotion = ((FrameworkElement)sender).DataContext as Promotion;

            if (MessageBoxComponent.deleteMessageBox("la promotion" + promotion.name + " " + promotion.years) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                PromotionDao.delete(promotion);

                promotions = completeTable();

                clearSearchInput();
            }

            Mouse.OverrideCursor = null;
        }

        private void goToCreateViewPromotion(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CreatePromotion());
        }

        private void goToSubjectsViewPromotion(object sender, RoutedEventArgs e)
        {
            Promotion promotion = ((FrameworkElement)sender).DataContext as Promotion;

            NavigationService.Navigate(new ViewSubjectPromotion(promotion));
        }

        private List<Promotion> completeTable()
        {
            List<Promotion> list = PromotionDao.getAll();

            dataGrid_showPromotion_listePromotion.ItemsSource = list;

            return list;
        }

        private void input_showPromotion_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Promotion> finalList = new List<Promotion>();

            string searchText = input_showPromotion_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                btn_showPromotion_deleteSearchBox.Visibility = Visibility.Visible;

                foreach (Promotion p in promotions)
                {
                    if (p.name.ToLower().Contains(searchText) || p.years.ToLower().Contains(searchText))
                    {
                        finalList.Add(p);
                    }
                }

                dataGrid_showPromotion_listePromotion.ItemsSource = finalList;
            }
            else
            {
                btn_showPromotion_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_showPromotion_listePromotion.ItemsSource = promotions;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void clearSearchInput()
        {
            input_showPromotion_searchBox.Clear();

            btn_showPromotion_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
