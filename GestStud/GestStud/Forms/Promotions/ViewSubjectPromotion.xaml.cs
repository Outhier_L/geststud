﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Forms.ResourcesDictionary.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Promotions
{
    /// <summary>
    /// Logique d'interaction pour ViewSubjectPromotion.xaml
    /// </summary>
    /// 
    public partial class ViewSubjectPromotion : Page
    {
        private Promotion p;
        public ViewSubjectPromotion(Promotion promotion)
        {
            InitializeComponent();

            txt_viewSubjectsPromotion_title.Text = "Liste de matières de la promotion " + promotion.name;

            p = promotion;

            dataGrid_viewSubjectsPromotion_listSubject.ItemsSource = p.subjects;
        }

        private void goToAddSubjects(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AddSubjects(p));
        }

        private void removeSubject(object sender, RoutedEventArgs e)
        {
            Subject subject = ((FrameworkElement)sender).DataContext as Subject;

            if (MessageBoxComponent.deleteMessageBox("supprimer la matière " + subject.name + " dans la promotion " + p.name) == MessageBoxResult.Yes)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                p = PromotionDao.removeSubjectsInPromotion(p, subject);

                dataGrid_viewSubjectsPromotion_listSubject.ItemsSource = p.subjects;

                clearSearchInput();
            }

            Mouse.OverrideCursor = null;
        }

        private void input_viewSubjectPromotion_searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Subject> finalList = new List<Subject>();

            string searchText = input_viewSubjectPromotion_searchBox.Text.ToLower();

            if (!searchText.Equals(""))
            {
                foreach (Subject s in p.subjects)
                {
                    btn_viewSubjectPromotion_deleteSearchBox.Visibility = Visibility.Visible;

                    if (s.name.ToLower().Contains(searchText) || s.coefficient.ToString().ToLower().Contains(searchText))
                    {
                        finalList.Add(s);
                    }

                    if (s.subjectGroup != null)
                    {
                        if (s.subjectGroup.name.ToLower().Contains(searchText) && !finalList.Contains(s))
                        {
                            finalList.Add(s);
                        }
                    }
                }

                dataGrid_viewSubjectsPromotion_listSubject.ItemsSource = finalList;
            }
            else
            {
                btn_viewSubjectPromotion_deleteSearchBox.Visibility = Visibility.Hidden;

                dataGrid_viewSubjectsPromotion_listSubject.ItemsSource = p.subjects;
            }
        }

        private void deleteSearchBox(object sender, RoutedEventArgs e)
        {
            clearSearchInput();
        }

        private void clearSearchInput()
        {
            input_viewSubjectPromotion_searchBox.Clear();

            btn_viewSubjectPromotion_deleteSearchBox.Visibility = Visibility.Hidden;
        }
    }
}
