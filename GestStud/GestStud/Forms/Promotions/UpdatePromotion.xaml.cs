﻿using FluentValidation.Results;
using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace GestStud.Forms.Promotions
{
    /// <summary>
    /// Logique d'interaction pour UpdatePromotion.xaml
    /// </summary>
    public partial class UpdatePromotion : Page
    {
        private readonly PromotionValidator validator;
        public UpdatePromotion(Promotion promotion)
        {
            InitializeComponent();

            DataContext = promotion;

            validator = new PromotionValidator();

            txt_updateType_title.Text = "Mise à jour de la promotion " + promotion.name;
        }

        private void updatePromotion(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            ValidationResult result = validator.Validate((Promotion)DataContext);

            if(result.IsValid)
            {
                PromotionDao.update((Promotion)DataContext);

                NavigationService.Navigate(new ShowPromotion());
            }

            Mouse.OverrideCursor = null;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
