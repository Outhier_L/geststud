﻿using GestStud.Dao;
using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestStud.Forms.Promotions
{
    /// <summary>
    /// Logique d'interaction pour AddSubjects.xaml
    /// </summary>
    public partial class AddSubjects : Page
    {
        public AddSubjects(Promotion promotion)
        {
            InitializeComponent();

            input_addSubjects_subjects.ItemsSource = SubjectDao.getDifferenceBetweenListSubjects(promotion.subjects.ToList()) ;

            DataContext = promotion;

            txt_addSubject_title.Text = "Ajouter des matières pour la promotion " + promotion.name + " " + promotion.years;
        }

        private void goBack(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void addSubjects(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            Promotion promotion = (Promotion)DataContext;

            foreach (Subject s in input_addSubjects_subjects.SelectedItems)
            {
                promotion.subjects.Add(s);
            }

            PromotionDao.addSubjectsInPromotion(promotion);

            NavigationService.Navigate(new ViewSubjectPromotion(promotion));

            Mouse.OverrideCursor = null;
        }
    }
}
