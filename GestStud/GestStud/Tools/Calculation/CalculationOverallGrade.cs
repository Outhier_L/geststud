﻿using GestStud.Dao;
using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Tools.Calculation
{
    public static class CalculationOverallGrade
    {

        public static Student getGeneralOverallGradeOfStudent(Student student, Reports report)
        {
            double generalOverallGrade = 0;

            int counter = 0;

            List<SubjectGroup> list = new List<SubjectGroup>();

            foreach (Subject s in student.promotion.subjects)
            {
               if(s.subjectGroup.name != "Sans groupe")
               {
                    if(!list.Contains(s.subjectGroup))
                    {
                        list.Add(s.subjectGroup);

                        double overallGrade = getOverallGradeOfSubjectGroup(s.subjectGroup, GradeDao.getGradeListFormReport(report, student.grades));

                        if(overallGrade != -1)
                        {
                            generalOverallGrade += overallGrade * s.subjectGroup.coefficient;
                            counter += s.subjectGroup.coefficient;
                        }
                    }
               }
               else
               {
                    double overallGrade = getOverallGradeOfSubject(GradeDao.getGradeListFormReport(report, student.grades), s);

                    if(overallGrade != -1)
                    {
                        generalOverallGrade += overallGrade * s.coefficient;
                        counter += s.coefficient;
                    }    
               }
            }

            student.overallAverage = Math.Round(generalOverallGrade / counter, 2);

            return student;
        }

        public static double getGeneralOverallGradeOfStudentContinue(Student student, Reports report)
        {
            double generalOverallGrade = 0;

            int counter = 0;

            List<SubjectGroup> list = new List<SubjectGroup>();

            foreach (Subject s in student.promotion.subjects)
            {

                if (s.subjectGroup.name != "Sans groupe" || s.name == "Livret de suivi")
                {
                    if (!list.Contains(s.subjectGroup))
                    {
                        list.Add(s.subjectGroup);

                        double overallGrade = getOverallGradeOfSubjectGroupContinue(s.subjectGroup, GradeDao.getGradeListFormReport(report, student.grades));

                        if (overallGrade != -1)
                        {
                            generalOverallGrade += overallGrade * s.subjectGroup.coefficient;
                            counter += s.subjectGroup.coefficient;
                        }
                    }
                }
                else if(s.subjectGroup.name == "Sans groupe")
                {
                    double overallGrade = getOverallGradeFromSubjectContinue(GradeDao.getGradeListFormReport(report, student.grades), s);

                    if (overallGrade != -1)
                    {
                        generalOverallGrade += overallGrade * s.coefficient;
                        counter += s.coefficient;
                    }
                }
            }

            return Math.Round(generalOverallGrade / counter, 2); ;
        }


        public static Student getInfoOverallGradeOfStudent(Student student, Reports report)
        {
            double generalOverallGrade = 0;

            int counter = 0;

            List<SubjectGroup> list = new List<SubjectGroup>();

            foreach (Subject s in student.promotion.subjects)
            {
                if (s.subjectGroup.name != "Sans groupe")
                {
                    if (!list.Contains(s.subjectGroup) && s.subjectGroup.info_grade == true)
                    {
                        list.Add(s.subjectGroup);

                        double overallGrade = getOverallGradeOfSubjectGroup(s.subjectGroup, GradeDao.getGradeListFormReport(report, student.grades));

                        if(overallGrade != -1)
                        {
                            generalOverallGrade += overallGrade * s.subjectGroup.coefficient;
                            counter += s.subjectGroup.coefficient;
                        }
                    }
                }
                else
                {
                    if(s.info_grade == true)
                    {
                        double overallGrade = getOverallGradeOfSubject(GradeDao.getGradeListFormReport(report, student.grades), s);

                        if(overallGrade != -1)
                        {
                            generalOverallGrade += overallGrade * s.coefficient;
                            counter += s.coefficient;
                        }
                    }
                }
            }

            student.infoOverallAverage = Math.Round(generalOverallGrade / counter, 2);

            return student;
        }

        public static double getOverallGradeFromPromotion(List<Student> students)
        {
            double generalOverallGrade = 0;

            int counter = 0;

            foreach (Student student in students)
            {
                if(student.overallAverage.Equals(double.NaN) == false) {
                    generalOverallGrade += student.overallAverage;
                    counter++;
                }
            }

            return Math.Round(generalOverallGrade / counter, 2);
        }

        public static double getOverallGradeFromPromotionForLivretSuivi(List<Student> students)
        {
            double generalOverallGrade = 0;

            int counter = 0;

            foreach(Student s in students)
            {
                foreach(Grade g in s.grades)
                {
                    if(g.subject.name == "Livret de suivi")
                    {
                        generalOverallGrade += g.value * g.subject.coefficient;
                        counter += g.subject.coefficient;
                    }
                }
            }

            return Math.Round(generalOverallGrade / counter, 2);
        }

        public static double getOverallGradeFromPromotionOfSubject(ICollection<Grade> list, Subject subject, Promotion promotion, Reports report)
        {
            List<Grade> subjectList = GradeDao.getGradeFromPromotionAndReport(promotion, list, report);

            int counter = 0;

            float average = 0;

            if (subjectList.Count > 0)
            {
                foreach (Grade g in subjectList)
                {
                    average += g.value * g.type.coefficient;
                    counter += g.type.coefficient;
                }

                return Math.Round(average / counter, 2);
            }

            return -1;

        }

        public static double getOverallGradeFromPromotionOfSubjectGroup(ICollection<Grade> list, SubjectGroup sg, Promotion promotion, Reports report)
        {
            int counter = 0;

            double average = 0;
            
            foreach (Subject s in sg.subjects)
            {
                double overallGrade = getOverallGradeFromPromotionOfSubject(list, s, promotion, report);

                if (overallGrade != -1)
                {
                    average += overallGrade * s.coefficient;
                    counter += s.coefficient;
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;

        }

        public static double getOverallGradeFromSubjectContinue(ICollection<Grade> list, Subject subject)
        {

            List<Grade> subjectList = GradeDao.getGradeListContinueFromSubject(subject, list);

            int counter = 0;

            float average = 0;

            if (subjectList.Count > 0)
            {
                foreach (Grade g in subjectList)
                {
                    average += g.value * g.type.coefficient;
                    counter += g.type.coefficient;
                }

                return Math.Round(average / counter, 2);
            }

            return -1;
        }

        public static double getOverallGradeFromSubjectExamen(ICollection<Grade> list, Subject subject)
        {

            List<Grade> subjectList = GradeDao.getGradeListExamenFromSubject(subject, list);

            int counter = 0;

            float average = 0;

            if (subjectList.Count > 0)
            {
                foreach (Grade g in subjectList)
                {
                    average += g.value * g.type.coefficient;
                    counter += g.type.coefficient;
                }

                return Math.Round(average / counter, 2);
            }

            return -1;
        }


        public static double getOverallGradeOfSubject(ICollection<Grade> list, Subject subject)
        {
            List<Grade> subjectList = GradeDao.getGradeListFromSubject(subject, list);

            int counter = 0;

            float average = 0;

            if(subjectList.Count > 0)
            {
                foreach (Grade g in subjectList)
                {
                    average += g.value * g.type.coefficient;
                    counter += g.type.coefficient;
                }

                return Math.Round(average / counter, 2);
            }

            return -1;
        }

        public static double getOverallGradeOfSubjectGroup(SubjectGroup subjectGroup, ICollection<Grade> list)
        {
            int counter = 0;

            double average = 0;

            foreach(Subject s in subjectGroup.subjects)
            {
                double overallGrade = getOverallGradeOfSubject(list, s);
                if(overallGrade != -1)
                {
                    average += overallGrade * s.coefficient;
                    counter += s.coefficient;
                }
            }

            if(counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;
            
        }

        public static double getOverallGradeOfSubjectGroupContinue(SubjectGroup subjectGroup, ICollection<Grade> list)
        {
            int counter = 0;

            double average = 0;

            foreach (Subject s in subjectGroup.subjects)
            {
                double overallGrade = getOverallGradeFromSubjectContinue(list, s);

                if (overallGrade != -1)
                {
                    average += overallGrade * s.coefficient;
                    counter += s.coefficient;
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;

        }

        public static double getOverallGradeOfSubjectGroupExamen(SubjectGroup subjectGroup, ICollection<Grade> list)
        {
            int counter = 0;

            double average = 0;

            foreach (Subject s in subjectGroup.subjects)
            {
                double overallGrade = getOverallGradeFromSubjectExamen(list, s);

                if (overallGrade != -1)
                {
                    average += overallGrade * s.coefficient;
                    counter += s.coefficient;
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;

        }

        public static double getOverallGradeOfSoutenanceOrale(ICollection<Grade> list)
        {
            int counter = 0;

            double average = 0;

            foreach(Grade g in list)
            {
                if(g.subject.name.Equals("Soutenance orale"))
                {
                    average += g.value * g.type.coefficient;
                    counter += g.type.coefficient;
                }
            }

            if(counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;
        }


        public static double getOverallGradeOfSoutenanceOraleForPromotion(Promotion promotion)
        {
            int counter = 0;

            double average = 0;

            foreach(Student s in promotion.students)
            {
                foreach (Grade g in s.grades)
                {
                    if (g.subject.name.Equals("Soutenance orale"))
                    {
                        average += g.value * g.type.coefficient;
                        counter += g.type.coefficient;
                    }
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;
        }

        public static double getOverallGradeOfProjetPedago(ICollection<Grade> list)
        {
            int counter = 0;

            double average = 0;

            foreach (Grade g in list)
            {
                if (g.subject.name.Equals("Projet pédagogique"))
                {
                    average += g.value * g.type.coefficient;
                    counter += g.type.coefficient;
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;
        }

        public static double getOverallGradeOfProjetPedagoForPromotion(Promotion promotion)
        {
            int counter = 0;

            double average = 0;

            foreach (Student s in promotion.students)
            {
                foreach(Grade g in s.grades)
                {
                    if (g.subject.name.Equals("Projet pédagogique"))
                    {
                        average += g.value * g.type.coefficient;
                        counter += g.type.coefficient;
                    }
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;
        }

        public static double getOverallGradeOfEvalEntre(ICollection<Grade> list)
        {
            int counter = 0;

            double average = 0;

            foreach (Grade g in list)
            {
                if (g.subject.name.Equals("Evaluation entreprise"))
                {
                    average += g.value * g.type.coefficient;
                    counter += g.type.coefficient;
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;
        }


        public static double getOverallGradeOfEvalEntreForPromotion(Promotion promotion)
        {
            int counter = 0;

            double average = 0;

            foreach(Student s in promotion.students)
            {
                foreach (Grade g in s.grades)
                {
                    if (g.subject.name.Equals("Evaluation entreprise"))
                    {
                        average += g.value * g.type.coefficient;
                        counter += g.type.coefficient;
                    }
                }
            }

            if (counter > 0)
            {
                return Math.Round(average / counter, 2);
            }

            return -1;
        }

        public static double getOverallGradeOfProjetEntre(ICollection<Grade> list)
        {
            double EvaluationEntreprise = getOverallGradeOfEvalEntre(list);
            double ProjetPedago = getOverallGradeOfSoutenanceOrale(list);

            if(EvaluationEntreprise != -1 && ProjetPedago != -1)
            {
                return Math.Round((EvaluationEntreprise + ProjetPedago) / 2, 2);
            }

            return -1;
        }

        public static double getFinalAverage(Student student)
        {
            double year1 = getGeneralOverallGradeOfStudent(student, Reports.Première_année).overallAverage;
            double year2 = getGeneralOverallGradeOfStudent(student, Reports.Deuxième_année).overallAverage;
            double certification = getGeneralOverallGradeOfStudent(student, Reports.Certification).overallAverage;

            return Math.Round((year1 + year2 + (certification * 2)) / 4, 2);

        }
    }
}
