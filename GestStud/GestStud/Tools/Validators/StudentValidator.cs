﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using GestStud.Entities;

namespace GestStud.Tools.Validators
{
    class StudentValidator : AbstractValidator<Student>
    {
        public StudentValidator()
        {
            RuleFor(s => s.first_name).NotEmpty().WithMessage("Le champ prénom ne doit pas être vide !");
            RuleFor(s => s.first_name).Length(1, 50).WithMessage("Le prénom ne doit pas excéder 50 caractères !");

            RuleFor(s => s.last_name).NotEmpty().WithMessage("Le champ nom ne doit pas être vide !");
            RuleFor(s => s.last_name).Length(1, 50).WithMessage("Le nom ne doit pas excéder 50 caractères !");

            RuleFor(s => s.promotion).NotEmpty().WithMessage("Veuillez selectionner une promotion !");
        }
    }
}
