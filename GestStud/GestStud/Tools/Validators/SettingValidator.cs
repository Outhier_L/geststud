﻿using FluentValidation;
using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Tools.Validators
{
    class SettingValidator : AbstractValidator<Setting>
    {
        public SettingValidator()
        {
            RuleFor(s => s.name).NotEmpty().WithMessage("Le champ Nom ne peut pas être vide !");
            RuleFor(s => s.name).Length(1, 50).WithMessage("Le nom ne doit pas excéder 50 caractères !");
            RuleFor(s => s.description).NotEmpty().WithMessage("Le champ Description ne peut pas être vide !");
           
        }
    }
}
