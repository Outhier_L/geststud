﻿using FluentValidation;
using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = GestStud.Entities.Type;

namespace GestStud.Tools.Validators
{
    class TypeValidator : AbstractValidator<Type>
    {
        public TypeValidator()
        {
            RuleFor(t => t.name).NotEmpty().WithMessage("Le champs Nom ne doit pas être vide !");
            RuleFor(t => t.name).Length(1, 50).WithMessage("Le nom ne doit pas excéder 50 caractères !");
            RuleFor(t => t.name).Must(dontMatchWith).WithMessage("Le nom existe déjà");
            RuleFor(t => t.coefficient).NotEmpty().WithMessage("Le champ Coefficient ne doit pas être vide !");

        }

        private bool dontMatchWith(string name)
        {
            if (name == "Entreprise" || name == "Examen")
            {
                return false;
            }

            return true;
        }

    }
}
