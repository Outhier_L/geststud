﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using GestStud.Entities;

namespace GestStud.Tools.Validators
{
    class SubjectsGroupValidator : AbstractValidator<SubjectGroup>
    {
        public SubjectsGroupValidator()
        {
            RuleFor(s => s.name).NotEmpty().WithMessage("Le champs nom ne peut pas être vide !");
            RuleFor(s => s.name).Length(1, 50).WithMessage("Le nom ne doit pas excéder 50 caractères !");
            RuleFor(s => s.name).Must(dontMatchWith).WithMessage("Ce nom existe déjà !");

            RuleFor(s => s.coefficient).NotEmpty().WithMessage("Le champs coefficient ne peut pas être vide !");

        }

        private bool dontMatchWith(string name)
        {
            if (name == "Sans groupe")
            {
                return false;
            }

            return true;
        }
    }
}
