﻿using FluentValidation;
using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Tools.Validators
{
    class PromotionValidator : AbstractValidator<Promotion>
    {
        public PromotionValidator()
        {
            RuleFor(p => p.name).NotEmpty().WithMessage("Le champ Nom ne peut pas être vide !");
            RuleFor(p => p.name).Length(1, 50).WithMessage("Le nom ne doit pas excéder 50 caractères !");
            RuleFor(p => p.years).NotEmpty().WithMessage("Le champ Année ne peut pas être vide !");
            RuleFor(p => p.years).Length(1, 50).WithMessage("L'année ne doit pas excéder 50 caractères !");
        }
    }
}
