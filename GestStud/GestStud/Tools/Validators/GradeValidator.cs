﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using GestStud.Dao;
using GestStud.Entities;

namespace GestStud.Tools.Validators
{
    class GradeValidator : AbstractValidator<Grade>
    {
        public GradeValidator()
        {
            RuleFor(g => g.description).NotEmpty().WithMessage("Le champs description ne peut pas être vide !");
            RuleFor(g => g.description).Length(1, 50).WithMessage("La description ne doit pas excéder 50 caractères !");

            RuleFor(g => g.value).LessThanOrEqualTo(20).WithMessage("La note ne peut pas être supérieur à 20 !");

            RuleFor(g => g.value).NotEmpty().WithMessage("Le champs Valeur de la note ne peut pas être vide !");
        }
    }
}
