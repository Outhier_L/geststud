﻿using GestStud.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace GestStud.Tools.Validators
{
    class SubjectValidator : AbstractValidator<Subject>
    {
        public SubjectValidator()
        {
            RuleFor(s => s.name).NotEmpty().WithMessage("Le champs nom ne peut pas être vide !");
            RuleFor(s => s.name).Length(1, 50).WithMessage("Le nom ne doit pas excéder 50 caractères !");
            RuleFor(s => s.name).Must(dontMatchWith).WithMessage("Le nom existe déjà !"); 

            RuleFor(s => s.coefficient).NotEmpty().WithMessage("Le champs coefficient ne peut pas être vide !");

            RuleFor(s => s.subjectGroup).NotEmpty().WithMessage("Le champs promotion ne peut pas être vide !");
        }


        private bool dontMatchWith(string name)
        {
            if (name == "Projet pédagogique" || name == "Evaluation entreprise" || name == "Soutenance orale" || name == "Livret de suivi")
            {
                return false;
            }

            return true;
        }

    }
}
