﻿using GestStud.Dao;
using GestStud.Entities;
using GestStud.Tools.Calculation;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NsExcel = Microsoft.Office.Interop.Excel;

namespace GestStud.Tools.Exports
{
    public static class ExportReportToExcel
    {
        private static NsExcel.Application excapp = new Microsoft.Office.Interop.Excel.Application();
        private static Workbook workbook;

        public static Workbook buildExcelReport(Student student, List<Grade> grades, Reports report, double classeAverage)
        {
            //http://amange.infofil.fr:8039/Releases/model_report.xlsx
            workbook = excapp.Workbooks.Open("http://amange.infofil.fr:8039/Releases/model_report.xlsx");

            var sheet = (NsExcel.Worksheet)workbook.Sheets[1];
            sheet.Name = student.first_name + " " + student.last_name + " " + report;

            sheet.Cells[4, "A"] = SettingDao.getSettingByCode(2).description;
            sheet.Cells[6, "A"] = SettingDao.getSettingByCode(1).description + " " + SettingDao.getSettingByCode(3).description + " Cedex";
            sheet.Cells[7, "A"] = SettingDao.getSettingByCode(4).description;

            sheet.Cells[5, "K"] = SettingDao.getSettingByCode(5).description;
            sheet.Cells[6, "K"] = SettingDao.getSettingByCode(6).description + " " + SettingDao.getSettingByCode(7).description;
            sheet.Cells[7, "K"] = SettingDao.getSettingByCode(8).description;

            sheet.Cells[12, "A"] = SettingDao.getSettingByCode(9).description;

            sheet.Cells[36, "F"] = "A " + SettingDao.getSettingByCode(3).description + " Le ";
            sheet.Cells[36, "H"] = DateTime.Today.ToString("dd MMMM yyyy");
            sheet.Cells[37, "F"] = "Responsable de dispositif : " + SettingDao.getSettingByCode(10).description;

            sheet.Cells[9, "A"] = "Releve de Notes - " + student.promotion.name + " " + student.promotion.years + " - " + report;
            sheet.Cells[14, "C"] = student.last_name;
            sheet.Cells[14, "D"] = student.first_name;

            sheet.Cells[31, "A"] = "Moyenne " + report;
            sheet.Cells[31, "E"] = student.overallAverage;
            sheet.Cells[22, "L"] = student.infoOverallAverage;
            sheet.Cells[22, "G"] = classeAverage;
            sheet.Cells[22, "F"] = CalculationOverallGrade.getGeneralOverallGradeOfStudentContinue(student, report);

            Grade livretSuivi = grades.FirstOrDefault(g => g.subject.name == "Livret de suivi");
            sheet.Cells[25, "A"] = livretSuivi.subject.name;
            sheet.Cells[25, "C"] = livretSuivi.subject.coefficient;
            sheet.Cells[25, "F"] = livretSuivi.value;
            sheet.Cells[25, "G"] = CalculationOverallGrade.getOverallGradeFromPromotionForLivretSuivi(student.promotion.students.ToList());

            sheet.Cells[27, "F"] = livretSuivi.value;
            sheet.Cells[27, "G"] = CalculationOverallGrade.getOverallGradeFromPromotionForLivretSuivi(student.promotion.students.ToList());

            int StartCell = 19;
            int EndCell = 21;

            int counter = 3;

            bool first = true;

            List<SubjectGroup> subjectGroups = new List<SubjectGroup>();

            foreach (Subject subject in student.promotion.subjects)
            {
                if(subject.name != "Livret de suivi")
                {
                    if (subject.subjectGroup.name == "Sans groupe")
                    {
                        if (CalculationOverallGrade.getOverallGradeOfSubject(grades, subject) != -1)
                        {

                            if (!first)
                            {
                                Range sourceRange = sheet.get_Range("A" + StartCell, "L" + EndCell);
                                Range destinationRange = sheet.get_Range("A" + (StartCell + counter), "L" + (EndCell + counter));
                                destinationRange.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown, sourceRange.Copy(System.Type.Missing));

                                string moyenneContinue = CalculationOverallGrade.getOverallGradeFromSubjectContinue(grades, subject).ToString();
                                if (moyenneContinue == "-1") moyenneContinue = "";

                                string moyenneExamen = CalculationOverallGrade.getOverallGradeFromSubjectExamen(grades, subject).ToString();
                                if (moyenneExamen == "-1") moyenneExamen = "";

                                string moyenneTotale = CalculationOverallGrade.getOverallGradeOfSubject(grades, subject).ToString();
                                if (moyenneTotale == "-1") moyenneTotale = "";

                                string moyenneClasse = CalculationOverallGrade.getOverallGradeFromPromotionOfSubject(subject.grades, subject, student.promotion, report).ToString();
                                if (moyenneClasse == "-1") moyenneClasse = "";

                                sheet.Cells[StartCell + counter, "A"] = subject.name;
                                sheet.Cells[StartCell + counter, "C"] = subject.coefficient;
                                sheet.Cells[StartCell + counter, "D"] = moyenneContinue;
                                sheet.Cells[StartCell + counter, "E"] = moyenneExamen;
                                sheet.Cells[StartCell + counter, "F"] = moyenneTotale;
                                sheet.Cells[StartCell + counter, "G"] = moyenneClasse;
                                sheet.Cells[StartCell + counter, "H"] = subject.remarque;

                                StartCell += counter;
                                EndCell += counter;
                            }
                            else
                            {

                                string moyenneContinue = CalculationOverallGrade.getOverallGradeFromSubjectContinue(grades, subject).ToString();
                                if (moyenneContinue == "-1") moyenneContinue = "";

                                string moyenneExamen = CalculationOverallGrade.getOverallGradeFromSubjectExamen(grades, subject).ToString();
                                if (moyenneExamen == "-1") moyenneExamen = "";

                                string moyenneTotale = CalculationOverallGrade.getOverallGradeOfSubject(grades, subject).ToString();
                                if (moyenneTotale == "-1") moyenneTotale = "";

                                string moyenneClasse = CalculationOverallGrade.getOverallGradeFromPromotionOfSubject(subject.grades, subject, student.promotion, report).ToString();
                                if (moyenneClasse == "-1") moyenneClasse = "";

                                sheet.Cells[StartCell, "A"] = subject.name;
                                sheet.Cells[StartCell, "C"] = subject.coefficient;
                                sheet.Cells[StartCell, "D"] = moyenneContinue;
                                sheet.Cells[StartCell, "E"] = moyenneExamen;
                                sheet.Cells[StartCell, "F"] = moyenneTotale;
                                sheet.Cells[StartCell, "G"] = moyenneClasse;
                                sheet.Cells[StartCell, "H"] = subject.remarque;

                                first = false;
                            }
                        }
                    }
                    else
                    {
                        if (CalculationOverallGrade.getOverallGradeOfSubjectGroup(subject.subjectGroup, grades) != -1)
                        {
                            if(!subjectGroups.Contains(subject.subjectGroup))
                            {
                                if (!first)
                                {
                                    Range sourceRange = sheet.get_Range("A" + StartCell, "L" + EndCell);
                                    Range destinationRange = sheet.get_Range("A" + (StartCell + counter), "L" + (EndCell + counter));
                                    destinationRange.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown, sourceRange.Copy(System.Type.Missing));

                                    string moyenneContinue = CalculationOverallGrade.getOverallGradeOfSubjectGroupContinue(subject.subjectGroup, grades).ToString();
                                    if (moyenneContinue == "-1") moyenneContinue = "";

                                    string moyenneExamen = CalculationOverallGrade.getOverallGradeOfSubjectGroupExamen(subject.subjectGroup, grades).ToString();
                                    if (moyenneExamen == "-1") moyenneExamen = "";

                                    string moyenneTotale = CalculationOverallGrade.getOverallGradeOfSubjectGroup(subject.subjectGroup, grades).ToString();
                                    if (moyenneTotale == "-1") moyenneTotale = "";

                                    string moyenneClasse = CalculationOverallGrade.getOverallGradeFromPromotionOfSubjectGroup(subject.grades, subject.subjectGroup, student.promotion, report).ToString();
                                    if (moyenneClasse == "-1") moyenneClasse = "";

                                    sheet.Cells[StartCell + counter, "A"] = subject.subjectGroup.name;
                                    sheet.Cells[StartCell + counter, "C"] = subject.subjectGroup.coefficient;
                                    sheet.Cells[StartCell + counter, "D"] = moyenneContinue;
                                    sheet.Cells[StartCell + counter, "E"] = moyenneExamen;
                                    sheet.Cells[StartCell + counter, "F"] = moyenneTotale;
                                    sheet.Cells[StartCell + counter, "G"] = moyenneClasse;
                                    sheet.Cells[StartCell + counter, "H"] = subject.subjectGroup.remarque;

                                    StartCell += counter;
                                    EndCell += counter;

                                    subjectGroups.Add(subject.subjectGroup);
                                }
                                else
                                {

                                    string moyenneContinue = CalculationOverallGrade.getOverallGradeOfSubjectGroupContinue(subject.subjectGroup, grades).ToString();
                                    if (moyenneContinue == "-1") moyenneContinue = "";

                                    string moyenneExamen = CalculationOverallGrade.getOverallGradeOfSubjectGroupExamen(subject.subjectGroup, grades).ToString();
                                    if (moyenneExamen == "-1") moyenneExamen = "";

                                    string moyenneTotale = CalculationOverallGrade.getOverallGradeOfSubjectGroup(subject.subjectGroup, grades).ToString();
                                    if (moyenneTotale == "-1") moyenneTotale = "";

                                    string moyenneClasse = CalculationOverallGrade.getOverallGradeFromPromotionOfSubjectGroup(subject.grades, subject.subjectGroup, student.promotion, report).ToString();
                                    if (moyenneClasse == "-1") moyenneClasse = "";

                                    sheet.Cells[StartCell, "A"] = subject.subjectGroup.name;
                                    sheet.Cells[StartCell, "C"] = subject.subjectGroup.coefficient;
                                    sheet.Cells[StartCell, "D"] = moyenneContinue;
                                    sheet.Cells[StartCell, "E"] = moyenneExamen;
                                    sheet.Cells[StartCell, "F"] = moyenneTotale;
                                    sheet.Cells[StartCell, "G"] = moyenneClasse;
                                    sheet.Cells[StartCell, "H"] = subject.subjectGroup.remarque;

                                    first = false;

                                    subjectGroups.Add(subject.subjectGroup);
                                }
                            }
                        }
                       
                    }
                }
            }
               


            return workbook;
        }

        public static Workbook buildCertifExcel(Student student, List<Grade> grades)
        {
            //http://amange.infofil.fr:8039/Releases/model_report.xlsx
            workbook = excapp.Workbooks.Open("http://amange.infofil.fr:8039/Releases/model_report_certif.xlsx");

            var sheet = (NsExcel.Worksheet)workbook.Sheets[1];
            sheet.Name = student.first_name + " " + student.last_name + " Certification";

            sheet.Cells[4, "A"] = SettingDao.getSettingByCode(2).description;
            sheet.Cells[6, "A"] = SettingDao.getSettingByCode(1).description + " " + SettingDao.getSettingByCode(3).description + " Cedex";
            sheet.Cells[7, "A"] = SettingDao.getSettingByCode(4).description;

            sheet.Cells[5, "K"] = SettingDao.getSettingByCode(5).description;
            sheet.Cells[6, "K"] = SettingDao.getSettingByCode(6).description + " " + SettingDao.getSettingByCode(7).description;
            sheet.Cells[7, "K"] = SettingDao.getSettingByCode(8).description;

            sheet.Cells[12, "A"] = SettingDao.getSettingByCode(9).description;

            sheet.Cells[41, "F"] = "A " + SettingDao.getSettingByCode(3).description + " Le ";
            sheet.Cells[41, "H"] = DateTime.Today.ToString("dd MMMM yyyy");
            sheet.Cells[42, "F"] = "Responsable de dispositif : " + SettingDao.getSettingByCode(10).description;

            sheet.Cells[9, "A"] = "Releve de Notes - " + student.promotion.name + " " + student.promotion.years + " - Certification";
            sheet.Cells[14, "C"] = student.last_name;
            sheet.Cells[14, "D"] = student.first_name;

            sheet.Cells[19, "A"] = "Soutenance orale";
            sheet.Cells[19, "C"] = 6;

            sheet.Cells[22, "A"] = "Projet pédagogique";
            sheet.Cells[22, "C"] = 3;

            sheet.Cells[25, "A"] = "Evaluation entreprise";
            sheet.Cells[25, "C"] = 6;

            sheet.Cells[31, "A"] = "Projet entreprise";

            string moyenneSoutenanceOrale = CalculationOverallGrade.getOverallGradeOfSoutenanceOrale(grades).ToString();
            if (moyenneSoutenanceOrale == "-1") moyenneSoutenanceOrale = "";
            sheet.Cells[19, "E"] = moyenneSoutenanceOrale;
            sheet.Cells[19, "F"] = moyenneSoutenanceOrale;

            string moyenneProjetPedago = CalculationOverallGrade.getOverallGradeOfProjetPedago(grades).ToString();
            if (moyenneProjetPedago == "-1") moyenneProjetPedago = "";
            sheet.Cells[22, "E"] = moyenneProjetPedago;
            sheet.Cells[22, "F"] = moyenneProjetPedago;


            string moyenneEvaluationEntre = CalculationOverallGrade.getOverallGradeOfEvalEntre(grades).ToString();
            if (moyenneEvaluationEntre == "-1") moyenneEvaluationEntre = "";
            sheet.Cells[25, "E"] = moyenneEvaluationEntre;
            sheet.Cells[25, "F"] = moyenneEvaluationEntre;

            string moyenneProjetEntre = CalculationOverallGrade.getOverallGradeOfProjetEntre(grades).ToString();
            if (moyenneProjetEntre == "-1") moyenneProjetEntre = "";
            sheet.Cells[31, "F"] = moyenneProjetEntre;

            sheet.Cells[36, "E"] = CalculationOverallGrade.getGeneralOverallGradeOfStudent(student, Reports.Première_année).overallAverage;

            sheet.Cells[38, "E"] = CalculationOverallGrade.getGeneralOverallGradeOfStudent(student, Reports.Deuxième_année).overallAverage;

            sheet.Cells[40, "E"] = CalculationOverallGrade.getGeneralOverallGradeOfStudent(student, Reports.Certification).overallAverage;

            sheet.Cells[42, "E"] = CalculationOverallGrade.getFinalAverage(student);
            sheet.Cells[28, "F"] = CalculationOverallGrade.getGeneralOverallGradeOfStudent(student, Reports.Certification).overallAverage;

            sheet.Cells[19, "G"] = CalculationOverallGrade.getOverallGradeOfSoutenanceOraleForPromotion(student.promotion);
            sheet.Cells[22, "G"] = CalculationOverallGrade.getOverallGradeOfProjetPedagoForPromotion(student.promotion);
            sheet.Cells[25, "G"] = CalculationOverallGrade.getOverallGradeOfEvalEntreForPromotion(student.promotion);

            return workbook; 

        }

        public static void closeExcel()
        {
            excapp.Quit();
        }

        public static void closeSheet()
        {
            workbook.Close(false);
        }
    }
}


