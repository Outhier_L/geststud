﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using MySql.Data.EntityFramework;
using GestStud.Entities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = GestStud.Entities.Type;

namespace GestStud.Tools
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class GestStudContext : DbContext
    {
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<SubjectGroup> SubjectGroups { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Grade> Grades { get; set; }

        public DbSet<Setting> Settings { get; set; }

        // Constructor - set name of connection
        public GestStudContext() : base("name=BDDConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }
    }
}
