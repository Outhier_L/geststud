﻿namespace GestStud.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.grades",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        description = c.String(nullable: false, unicode: false),
                        value = c.Single(nullable: false),
                        report = c.Int(nullable: false),
                        created_at = c.DateTime(nullable: false, precision: 0),
                        updated_at = c.DateTime(nullable: false, precision: 0),
                        student_id = c.Int(nullable: false),
                        subject_id = c.Int(nullable: false),
                        type_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.students", t => t.student_id, cascadeDelete: true)
                .ForeignKey("dbo.subjects", t => t.subject_id, cascadeDelete: true)
                .ForeignKey("dbo.types", t => t.type_id, cascadeDelete: true)
                .Index(t => t.student_id)
                .Index(t => t.subject_id)
                .Index(t => t.type_id);
            
            CreateTable(
                "dbo.students",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        first_name = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        last_name = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        created_at = c.DateTime(nullable: false, precision: 0),
                        updated_at = c.DateTime(nullable: false, precision: 0),
                        promotion_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.promotions", t => t.promotion_id, cascadeDelete: true)
                .Index(t => t.promotion_id);
            
            CreateTable(
                "dbo.promotions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        years = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        is_active = c.Boolean(nullable: false),
                        created_at = c.DateTime(nullable: false, precision: 0),
                        updated_at = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.subjects",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        coefficient = c.Int(nullable: false),
                        info_grade = c.Boolean(nullable: false),
                        created_at = c.DateTime(nullable: false, precision: 0),
                        updated_at = c.DateTime(nullable: false, precision: 0),
                        subjectGroup_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.subjects_groups", t => t.subjectGroup_id, cascadeDelete: true)
                .Index(t => t.subjectGroup_id);
            
            CreateTable(
                "dbo.subjects_groups",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        coefficient = c.Int(nullable: false),
                        info_grade = c.Boolean(nullable: false),
                        created_at = c.DateTime(nullable: false, precision: 0),
                        updated_at = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.types",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        coefficient = c.Int(nullable: false),
                        created_at = c.DateTime(nullable: false, precision: 0),
                        updated_at = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        code = c.Int(nullable: false),
                        description = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.code, unique: true);
            
            CreateTable(
                "dbo.SubjectPromotions",
                c => new
                    {
                        Subject_id = c.Int(nullable: false),
                        Promotion_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Subject_id, t.Promotion_id })
                .ForeignKey("dbo.subjects", t => t.Subject_id, cascadeDelete: true)
                .ForeignKey("dbo.promotions", t => t.Promotion_id, cascadeDelete: true)
                .Index(t => t.Subject_id)
                .Index(t => t.Promotion_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.grades", "type_id", "dbo.types");
            DropForeignKey("dbo.grades", "subject_id", "dbo.subjects");
            DropForeignKey("dbo.grades", "student_id", "dbo.students");
            DropForeignKey("dbo.students", "promotion_id", "dbo.promotions");
            DropForeignKey("dbo.subjects", "subjectGroup_id", "dbo.subjects_groups");
            DropForeignKey("dbo.SubjectPromotions", "Promotion_id", "dbo.promotions");
            DropForeignKey("dbo.SubjectPromotions", "Subject_id", "dbo.subjects");
            DropIndex("dbo.SubjectPromotions", new[] { "Promotion_id" });
            DropIndex("dbo.SubjectPromotions", new[] { "Subject_id" });
            DropIndex("dbo.Settings", new[] { "code" });
            DropIndex("dbo.subjects", new[] { "subjectGroup_id" });
            DropIndex("dbo.students", new[] { "promotion_id" });
            DropIndex("dbo.grades", new[] { "type_id" });
            DropIndex("dbo.grades", new[] { "subject_id" });
            DropIndex("dbo.grades", new[] { "student_id" });
            DropTable("dbo.SubjectPromotions");
            DropTable("dbo.Settings");
            DropTable("dbo.types");
            DropTable("dbo.subjects_groups");
            DropTable("dbo.subjects");
            DropTable("dbo.promotions");
            DropTable("dbo.students");
            DropTable("dbo.grades");
        }
    }
}
