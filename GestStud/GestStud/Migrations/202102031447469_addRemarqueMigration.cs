﻿namespace GestStud.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRemarqueMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.subjects", "remarque", c => c.String(unicode: false));
            AddColumn("dbo.subjects_groups", "remarque", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.subjects_groups", "remarque");
            DropColumn("dbo.subjects", "remarque");
        }
    }
}
