﻿using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Entities
{
    [Serializable]
    [Table("promotions")]
    public class Promotion : INotifyPropertyChanged, IDataErrorInfo
    {
        [NotMapped]
        private PromotionValidator validator;

        public Promotion()
        {
            validator = new PromotionValidator();
        }

        [Column("id")]
        [Key]
        public int id { get; set; }

        [Column("name")]
        [MaxLength(50), Required]
        public string name { get; set; }

        [Column("years")]
        [MaxLength(50), Required]
        public string years { get; set; }

        [Column("is_active")]
        public Boolean is_active { get; set; }

        [Column("created_at")]
        public DateTime created_at { get; set; }

        [Column("updated_at")]
        public DateTime updated_at { get; set; }

        public ICollection<Student> students { get; set; }
        public virtual ICollection<Subject> subjects { get; set; }

        [NotMapped]
        public string this[string columnName]
        {
            get
            {
                var firstOrDefault = validator.Validate(this).Errors.FirstOrDefault(lol => lol.PropertyName == columnName);
                if (firstOrDefault != null)
                    return validator != null ? firstOrDefault.ErrorMessage : "";
                return "";
            }
        }
        
        [NotMapped]
        public string Error
        {
              get
            {
                if (validator != null)
                {
                    var results = validator.Validate(this);
                    if (results != null && results.Errors.Any())
                    {
                        var errors = string.Join(Environment.NewLine, results.Errors.Select(x => x.ErrorMessage).ToArray());
                        return errors;
                    }
                }
                return string.Empty;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
