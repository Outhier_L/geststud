﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestStud.Dao;

namespace GestStud.Entities
{
    class ReportGrade
    {
        public String name { get; set; }

        public String subjectGroupName { set; get; }

        public String description { get; set; }

        public int coefficient { get; set; }

        public float grade { set; get; }

        public float average { set; get; }


        public ReportGrade(Grade g, Promotion promo)
        {
            this.name = g.subject.name;
            this.subjectGroupName = g.subject.subjectGroup.name;
            this.coefficient = g.type.coefficient;
            this.grade = g.value;
            this.description = g.description;

            List<Grade> gradesSubject = GradeDao.getGradeListFromSubjectReport(g.subject);
            List<Grade> gradesPromo = GradeDao.getGradeListFromSubjectReport(g.subject);

            List<Grade> gradesAverage = new List<Grade>();

            this.average = 0;
            int count = 0;


            foreach (Student s in promo.students)
            {
                foreach (Grade gradeTest in gradesSubject)
                {
                    if (gradeTest.student.id == s.id)
                    {
                        gradesAverage.Add(gradeTest);
                    }
                }
            }

            foreach(Grade gradeAverage in gradesAverage)
            {
                if (description == gradeAverage.description)
                {
                    this.average = (average + gradeAverage.value);
                    count++;
                }
            }

            this.average = this.average / count;
        }
    }
}
