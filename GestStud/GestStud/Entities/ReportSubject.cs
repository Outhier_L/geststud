﻿using GestStud.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Entities
{
    class ReportSubject
    {
        public String name { get; set; }
        public String nameGroup { get; set; }
        public String remarque { get; set; }
        public int coefficient { get; set; }
        public double average { get; set; }

        public ReportSubject(Grade g)
        {
            this.name = g.subject.name;
            this.nameGroup = g.subject.subjectGroup.name;
            this.remarque = g.subject.remarque;
            this.coefficient = g.subject.coefficient;

            List<Grade> gradeSubjects = GradeDao.getGradeListFromSubjectReport(g.subject);
            this.average = 0;
            int coeffGlo = 0;

            foreach (Grade gradeTest in gradeSubjects)
            {
                if (g.student.id == gradeTest.student.id)
                {
                    this.average = (average + gradeTest.value * gradeTest.type.coefficient);
                    coeffGlo = (coeffGlo + gradeTest.type.coefficient);
                }
            }
            this.average = Math.Round(this.average / coeffGlo, 2);
        }
    }
}
