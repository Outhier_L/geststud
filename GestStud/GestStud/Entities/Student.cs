﻿using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Entities
{
    [Table("students")]
    public class Student : INotifyPropertyChanged, IDataErrorInfo
    {

        [NotMapped]
        private readonly StudentValidator validator;

        public Student()
        {
            validator = new StudentValidator();
        }

        [Column("id")]
        [Key]
        public int id { get; set; }
    
        [Column("first_name")]
        [MaxLength(50), Required]
        public string first_name { get; set; }

        [Column("last_name")]
        [MaxLength(50), Required]
        public string last_name { get; set; }

        [Column("promotion_id")]
        [Required]
        public Promotion promotion { get; set; }

        [Column("created_at")]
        public DateTime created_at { get; set; }

        [Column("updated_at")]
        public DateTime updated_at { get; set; }

        [NotMapped]
        public double overallAverage { get; set; }

        [NotMapped]
        public double infoOverallAverage { get; set; }

        public ICollection<Grade> grades { get; private set; }

        [NotMapped]
        public string this[string columnName]
        {
            get
            {
                var firstOrDefault = validator.Validate(this).Errors.FirstOrDefault(lol => lol.PropertyName == columnName);
                if (firstOrDefault != null)
                    return validator != null ? firstOrDefault.ErrorMessage : "";
                return "";
            }
        }

        [NotMapped]
        public string Error
        {
            get
            {
                if (validator != null)
                {
                    var results = validator.Validate(this);
                    if (results != null && results.Errors.Any())
                    {
                        var errors = string.Join(Environment.NewLine, results.Errors.Select(x => x.ErrorMessage).ToArray());
                        return errors;
                    }
                }
                return string.Empty;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
