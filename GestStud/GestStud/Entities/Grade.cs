﻿using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Entities
{
    public enum Reports
    {
        Première_année,
        Deuxième_année,
        Certification 
    }

    [Serializable]
    [Table("grades")]
    public class Grade : INotifyPropertyChanged, IDataErrorInfo
    {
        [NotMapped]
        private GradeValidator validator;

        public Grade()
        {
            validator = new GradeValidator();
        }

        [Column("id")]
        [Key]
        public int id { get; set; }

        [Column("description")]
        [Required]
        public string description { get; set; }

        [Column("value")]
        [Required]
        public float value { get; set; }

        [Column("report")]
        [Required]
        public Reports report { get; set; }

        [Column("subject_id")]
        [Required]
        public Subject subject { get; set; }

        [Column("student_id")]
        [Required]
        public Student student { get; set; }

        [Column("type_id")]
        [Required]
        public Type type { get; set; }

        [Column("created_at")]
        public DateTime created_at { get; set; }

        [Column("updated_at")]
        public DateTime updated_at { get; set; }

        [NotMapped]
        public string this[string columnName]
        {
            get
            {
                var firstOrDefault = validator.Validate(this).Errors.FirstOrDefault(lol => lol.PropertyName == columnName);
                if (firstOrDefault != null)
                    return validator != null ? firstOrDefault.ErrorMessage : "";
                return "";
            }
        }

        [NotMapped]
        public string Error
        {
            get
            {
                if (validator != null)
                {
                    var results = validator.Validate(this);
                    if (results != null && results.Errors.Any())
                    {
                        var errors = string.Join(Environment.NewLine, results.Errors.Select(x => x.ErrorMessage).ToArray());
                        return errors;
                    }
                }
                return string.Empty;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

       
}
