﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestStud.Dao;

namespace GestStud.Entities
{
    class ReportSubjectGroups
    {
        public String name { get; set; }
        public String remarque { get; set; }
        public int coefficient { get; set; }
        public double average { get; set; }
        public List<ReportSubject> reportSubjects { get; set; }

        public ReportSubjectGroups(List<ReportSubject> listSubjects, ReportSubject reportSubject)
        {
            Subject s = SubjectDao.getByName(reportSubject.name);
            this.name = s.subjectGroup.name;
            this.remarque = s.subjectGroup.remarque;
            this.coefficient = s.subjectGroup.coefficient;

            reportSubjects = new List<ReportSubject>();

            foreach (ReportSubject rs in listSubjects)
            {
                if (reportSubject.nameGroup == "Sans groupe")
                {
                    break;
                }
                if(rs.nameGroup == this.name)
                {
                    if (reportSubjects.Contains(rs)) {
                        break;
                    }
                    else
                    {
                        reportSubjects.Add(rs);
                    }
                }
            }

            this.average = 0;
            int coeffGlo = 0;

            foreach (ReportSubject subject in reportSubjects)
            {
                this.average = (average + subject.average * subject.coefficient);
                coeffGlo = (coeffGlo + subject.coefficient);
            }
            this.average = Math.Round(this.average / coeffGlo, 2);
        }
    }
}
