﻿using GestStud.Tools.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Entities
{
    [Serializable]
    [Table("subjects")]
    public class Subject : INotifyPropertyChanged, IDataErrorInfo
    {
        [NotMapped]
        private readonly SubjectValidator validator;

        public Subject()
        {
            validator = new SubjectValidator();
        }

        [Column("id")]
        [Key]
        public int id { get; set; }

        [Column("name")]
        [MaxLength(50), Required]
        public string name { get; set; }

        [Column("coefficient")]
        [Required]
        public int coefficient { get; set; }

        [Column("info_grade")]
        [Required]
        public bool info_grade { get; set; }

        [Column("remarque")]
        public string remarque { get; set; }

        [Column("subject_group_id")]
        [Required]
        public SubjectGroup subjectGroup { get; set; }

        [Column("created_at")]
        public DateTime created_at { get; set; }

        [Column("updated_at")]
        public DateTime updated_at { get; set; }

        public virtual ICollection<Promotion> Promotions { get; set; }

        public ICollection<Grade> grades { get; set; }

        [NotMapped]
        public string this[string columnName]
        {
            get
            {
                var firstOrDefault = validator.Validate(this).Errors.FirstOrDefault(lol => lol.PropertyName == columnName);
                if (firstOrDefault != null)
                    return validator != null ? firstOrDefault.ErrorMessage : "";
                return "";
            }
        }

        [NotMapped]
        public string Error
        {
            get
            {
                if (validator != null)
                {
                    var results = validator.Validate(this);
                    if (results != null && results.Errors.Any())
                    {
                        var errors = string.Join(Environment.NewLine, results.Errors.Select(x => x.ErrorMessage).ToArray());
                        return errors;
                    }
                }
                return string.Empty;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
