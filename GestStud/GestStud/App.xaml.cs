﻿using GestStud.Dao;
using GestStud.Forms;
using GestStud.Forms.Frames;
using System.Threading.Tasks;
using System.Windows;
using Squirrel;
using System;
using GestStud.Forms.ResourcesDictionary.Components;
using System.Threading;

namespace GestStud
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        SpalshScreen sp;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            sp = new SpalshScreen();
            MainWindow = sp;
            sp.Show();

            Task.Factory.StartNew(() =>
            {
                sp.SetText("Vérification des mises à jours...");
                if (!checkUpdate())
                {
                    sp.SetText("Chargement des données...");
                    PromotionDao.getAll();

                    Dispatcher.Invoke(() =>
                    {
                        Frame frame = new Frame();
                        MainWindow = frame;
                        frame.Show();
                        sp.Close();
                    });
                }
            });

        }

        private bool checkUpdate()
        {
                using (var mgr = new UpdateManager("http://amange.infofil.fr:8039/Releases/", "Geststud"))
                {
                    try
                    {
                        var updateInfo = mgr.CheckForUpdate().Result;
                        if(mgr.IsInstalledApp)
                        {
                            if (updateInfo.CurrentlyInstalledVersion.Version < updateInfo.FutureReleaseEntry.Version)
                            {
                                if (MessageBoxComponent.updateMessageBox($"{updateInfo.FutureReleaseEntry.Version}") == MessageBoxResult.Yes)
                                {
                                    sp.initializeDownloadScreen();
                                    sp.SetText("Téléchargement des mises à jour ...");

                                    mgr.DownloadReleases(updateInfo.ReleasesToApply, sp.UpdateProgress)
                                        .ContinueWith((t) =>
                                        {
                                            Application.Current.Dispatcher.Invoke(() =>
                                            {
                                                sp.SetText("Installation des mises à jour ...");
                                                sp.UpdateProgress(0);
                                            });
                                            mgr.ApplyReleases(updateInfo, sp.UpdateProgress)
                                             .ContinueWith((x) =>
                                             {
                                                 mgr.Dispose();

                                                 sp.CanClose = true;
                                                 UpdateManager.RestartApp();
                                                 return true;
                                             });
                                        });
                                }
                                else
                                    return false;
                            }
                            return false;
                        }
                        return false;

                    }
                    catch (Exception exc)
                    {
                        //log
                        Console.WriteLine(exc);
                        return false;
                    }
            }

        }

    }
}
