﻿using GestStud.Entities;
using GestStud.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Type = GestStud.Entities.Type;

namespace GestStud.Dao
{
    public static class GradeDao
    {
        public static List<Grade> getGradeListFormReport(Reports report, ICollection<Grade> list)
        {
            List<Grade> reportList = new List<Grade>();

            foreach (Grade g in list)
            {
                if (g.report == report)
                {
                    reportList.Add(g);
                }
            }

            return reportList;
        }

        public static List<Grade> getGradeListExamenFromSubject(Subject subject, ICollection<Grade> list)
        {
            List<Grade> subjectList = new List<Grade>();

            foreach(Grade g in list)
            {
                if(g.subject == subject && g.type.name == "Examen")
                {
                    subjectList.Add(g);
                }
            }

            return subjectList;
        }

        public static List<Grade> getGradeListContinueFromSubject(Subject subject, ICollection<Grade> list)
        {
            List<Grade> subjectList = new List<Grade>();

            foreach (Grade g in list)
            {
                if (g.subject == subject && g.type.name != "Examen" && g.type.name != "Entreprise")
                {
                    subjectList.Add(g);
                }
            }

            return subjectList;
        }

        public static List<Grade> getGradeListFromSubjectReport(Subject subject)
        {
            List<Grade> subjectList = new List<Grade>();
            using (var ctx = new GestStudContext())
            {
                List<Grade> grades = ctx.Grades
                    .Include("student")
                    .Include("type")
                    .Where(g => g.subject.id == subject.id)
                    .ToList();

                ctx.Dispose();

                return grades;
            }
        }
        public static List<Grade> getGradeListFromSubject(Subject subject, ICollection<Grade> list)
        {
            List<Grade> subjectList = new List<Grade>();

            foreach (Grade g in list)
            {
                if (g.subject == subject && g.type.name != "Entreprise")
                {
                    subjectList.Add(g);
                }
            }

            return subjectList;
        }


        public static List<Grade> getGradeFromPromotionFromSubject(Promotion promotion, Subject subject)
        {
            List<Grade> grades = new List<Grade>();
            List<Grade> finalGrade = new List<Grade>();

            using (var ctx = new GestStudContext())
            {
                foreach (Student s in promotion.students)
                {
                    grades = ctx.Grades
                    .Include("student")
                    .Include("type")
                    .Where(g => g.student.id == s.id && g.subject.id == subject.id)
                    .ToList();

                    foreach(Grade g in grades)
                    {
                        finalGrade.Add(g);
                    }
                }
                ctx.Dispose();

                return finalGrade;
            }
        }

        public static List<Grade> getGradeFromPromotionAndReport(Promotion promotion, ICollection<Grade> list, Reports report)
        {
            List<Grade> grades = new List<Grade>();

            foreach (Student s in promotion.students)
            {
                foreach (Grade g in list)
                {
                    if (g.student.id == s.id && g.report == report)
                    {
                        grades.Add(g);
                    }
                }
            }
            return grades;
        }

        public static Grade getGradeWithDescription(string description)
        {
            using (var ctx = new GestStudContext())
            {
                Grade grade = ctx.Grades.Where(g => g.description == description).FirstOrDefault();
                ctx.Dispose();

                return grade;
            }
        }

        public static Grade create(Grade grade)
        {
            using (var ctx = new GestStudContext())
            {
                Subject subject = ctx.Subjects.Where(s => s.id == grade.subject.id).First();

                Type type = ctx.Types.Where(t => t.id == grade.type.id).First();

                Student student = ctx.Students.Where(s => s.id == grade.student.id).First();

                grade.subject = subject;
                grade.type = type;
                grade.student = student;

                grade.created_at = grade.updated_at = DateTime.Now;

                ctx.Grades.Add(grade);
                ctx.SaveChanges();
                ctx.Dispose();

                return grade;
            }
        }

        public static Grade update(Grade grade)
        {
            using (var ctx = new GestStudContext())
            {
                Grade gradeUpdated = ctx.Grades.Where(p => p.id == grade.id).First();

                Subject subject = ctx.Subjects.Where(s => s.id == grade.subject.id).First();

                Type type = ctx.Types.Where(t => t.id == grade.type.id).First();

                Student student = ctx.Students.Where(s => s.id == grade.student.id).First();

                gradeUpdated.subject = subject;
                gradeUpdated.type = type;
                gradeUpdated.student = student;
                gradeUpdated.description = grade.description;
                gradeUpdated.value = grade.value;
                gradeUpdated.report = grade.report;

                grade.updated_at = DateTime.Now;

                ctx.SaveChanges();
                ctx.Dispose();

                return grade;
            }
        }

        public static bool delete(Grade grade)
        {
            using (var ctx = new GestStudContext())
            {
                Grade gradeDeleted = ctx.Grades.Where(u => u.id == grade.id).First();

                ctx.Grades.Remove(gradeDeleted);
                ctx.SaveChanges();
                ctx.Dispose();

                return true;
            }
        }
    }
}
