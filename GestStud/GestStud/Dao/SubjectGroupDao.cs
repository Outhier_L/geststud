﻿using System;
using System.Collections.Generic;
using System.Linq;
using GestStud.Entities;
using System.Text;
using System.Threading.Tasks;
using GestStud.Tools;

namespace GestStud.Dao
{
    public static class SubjectGroupDao
    {
        public static SubjectGroup getById(int id)
        {
            using (var ctx = new GestStudContext())
            {
                SubjectGroup subjectGroup = ctx.SubjectGroups.Where(u => u.id == id).First();
                ctx.Dispose();

                if (subjectGroup != null)
                {
                    return subjectGroup;
                }
                return null;
            }
        }

        public static List<SubjectGroup> getAll()
        {

            using (var ctx = new GestStudContext())
            {
                List<SubjectGroup> stus = ctx.SubjectGroups.Include("Subjects").ToList();
                ctx.Dispose();

                return stus;
            }
        }

        public static SubjectGroup create(SubjectGroup subjectGroup)
        {
            using (var ctx = new GestStudContext())
            {
                subjectGroup.created_at = subjectGroup.updated_at = DateTime.Now;

                ctx.SubjectGroups.Add(subjectGroup);
                ctx.SaveChanges();
                ctx.Dispose();

                return subjectGroup;
            }
        }

        public static void addSubjectsInSubjectGroup(SubjectGroup subjectGroup)
        {
            using (var ctx = new GestStudContext())
            {
                SubjectGroup subjectGroupUpdated = ctx.SubjectGroups
                    .Include("Subjects")
                    .Where(p => p.id == subjectGroup.id)
                    .First();

                foreach (Subject s in subjectGroup.subjects)
                {
                    Subject subject = ctx.Subjects.Where(u => u.id == s.id).First();

                    subjectGroupUpdated.subjects.Add(subject);
                }

                ctx.SaveChanges();
                ctx.Dispose();
            }
        }

        public static SubjectGroup removeSubjectsInSubjectGroup(SubjectGroup subjectGroup, Subject subject)
        {
            using (var ctx = new GestStudContext())
            {
                SubjectGroup subjectGroupUpdated = ctx.SubjectGroups.Where(p => p.id == subjectGroup.id).First();
                SubjectGroup sansgroupe = ctx.SubjectGroups.Where(sg => sg.name == "Sans groupe").First();

                Subject subjectUpdated = ctx.Subjects.Where(u => u.id == subject.id).First();

                subjectGroupUpdated.subjects.Remove(subjectUpdated);
                subjectUpdated.subjectGroup = subjectGroup;

                ctx.SaveChanges();
                ctx.Dispose();

                return subjectGroupUpdated;
            }
        }


        public static SubjectGroup update(SubjectGroup subjectGroup)
        {
            using (var ctx = new GestStudContext())
            {
                SubjectGroup subjectGroupUpdated = ctx.SubjectGroups.Where(u => u.id == subjectGroup.id).First();

                subjectGroupUpdated.name = subjectGroup.name;
                subjectGroupUpdated.coefficient = subjectGroup.coefficient;
                subjectGroupUpdated.info_grade = subjectGroup.info_grade;
                subjectGroupUpdated.updated_at = DateTime.Now;
                subjectGroupUpdated.remarque = subjectGroup.remarque;

                ctx.SaveChanges();
                ctx.Dispose();

                return subjectGroup;
            }
        }

        public static bool delete(SubjectGroup sg)
        {
            using (var ctx = new GestStudContext())
            {
                SubjectGroup subjectGroup = ctx.SubjectGroups.Where(u => u.id == sg.id).First();

                ctx.SubjectGroups.Remove(subjectGroup);
                ctx.SaveChanges();
                ctx.Dispose();

                return true;
            }
        }
    }
}
