﻿using GestStud.Entities;
using GestStud.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Dao
{
    public static class SettingDao
    {
        public static List<Setting> getAll()
        {
            using (var ctx = new GestStudContext())
            {
                List<Setting> settings = ctx.Settings.ToList();
                ctx.Dispose();

                return settings;
            }
        }

        public static Setting getSettingByCode(int code)
        {
            using (var ctx = new GestStudContext())
            {
                Setting settings = ctx.Settings
                    .Where(s => s.code == code)
                    .First();
                ctx.Dispose();

                return settings;
            }
        }
        public static Setting update(Setting setting)
        {
            using (var ctx = new GestStudContext())
            {
                Setting settingUpdated = ctx.Settings.Where(s => s.id == setting.id).First();

                settingUpdated.description = setting.description;

                ctx.SaveChanges();
                ctx.Dispose();

                return setting;
            }
        }

    }
}
