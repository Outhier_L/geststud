﻿using System;
using System.Collections.Generic;
using System.Linq;
using GestStud.Entities;
using GestStud.Tools;
using System.Text;
using System.Data.Entity;


namespace GestStud.Dao
{
    public static class StudentDao
    {
        public static Student getById(int id)
        {
            using (var ctx = new GestStudContext())
            {
                Student student = ctx.Students.Where(u => u.id == id).First();
                ctx.Dispose();

                if (student != null)
                {
                    return student;
                }
                return null;
            }
        }

        public static List<Student> getAll()
        {
           
            using (var ctx = new GestStudContext())
            {
                List<Student> stus = ctx.Students.Include(s => s.promotion).ToList();
                ctx.Dispose();

                return stus;
            }
        }

        public static Student create(Student student)
        {
            using (var ctx = new GestStudContext())
            {
                Promotion promo = ctx.Promotions.Where(p => p.id == student.promotion.id).First();

                student.promotion = promo;
                student.created_at = student.updated_at = DateTime.Now;

                ctx.Students.Add(student);
                ctx.SaveChanges();
                ctx.Dispose();

                return student;
            }
        }

        public static Student update(Student student)
        {
            using (var ctx = new GestStudContext())
            {
                Student studentUpdated = ctx.Students.Where(u => u.id == student.id).First();
                Promotion promo = ctx.Promotions.Where(p => p.id == student.promotion.id).First();

                studentUpdated.first_name = student.first_name;
                studentUpdated.last_name = student.last_name;
                studentUpdated.promotion = promo;
                studentUpdated.updated_at = DateTime.Now;

                ctx.SaveChanges();
                ctx.Dispose();

                return student;
            }
        }

        public static bool delete(Student student)
        {
            using (var ctx = new GestStudContext())
            {
                Student studentDeleted = ctx.Students.Where(u => u.id == student.id).First();

                ctx.Students.Remove(studentDeleted);
                ctx.SaveChanges();
                ctx.Dispose();

                return true;
            }
        }
    }
}
