﻿using GestStud.Entities;
using GestStud.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Type = GestStud.Entities.Type;

namespace GestStud.Dao
{
    public static class TypeDao
    {
        public static Type getById(int id)
        {
            using (var ctx = new GestStudContext())
            {
                Type type = ctx.Types.Where(u => u.id == id).First();
                ctx.Dispose();

                if (type != null)
                {
                    return type;
                }
                return null;
            }
        }

        public static List<Type> getAll()
        {
            using (var ctx = new GestStudContext())
            {
                List<Type> list = ctx.Types.ToList();
                ctx.Dispose();

                return list;
            }
        }

        public static Type create(Type type)
        {
            using (var ctx = new GestStudContext())
            {
                type.created_at = type.updated_at = DateTime.Now;

                ctx.Types.Add(type);
                ctx.SaveChanges();
                ctx.Dispose();

                return type;
            }
        }

        public static Type update(Type type)
        {
            using (var ctx = new GestStudContext())
            {
                Type typeUpdated = ctx.Types.Where(u => u.id == type.id).First();

                typeUpdated.name = type.name;
                typeUpdated.coefficient = type.coefficient;
                typeUpdated.updated_at = DateTime.Now;

                ctx.SaveChanges();
                ctx.Dispose();

                return typeUpdated;
            }
        }

        public static bool delete(Type type)
        {
            using (var ctx = new GestStudContext())
            {
                Type typeDeleted = ctx.Types.Where(u => u.id == type.id).First();

                ctx.Types.Remove(typeDeleted);
                ctx.SaveChanges();
                ctx.Dispose();

                return true;
            }
        }
    }
}
