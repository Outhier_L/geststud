﻿using GestStud.Entities;
using GestStud.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace GestStud.Dao
{
    public static class PromotionDao
    {
        public static Promotion getById(int id)
        {
            using (var ctx = new GestStudContext())
            {
                Promotion promotion = ctx.Promotions.Where(p => p.id == id)
                    .Include("students")
                    .Include("students.grades")
                    .Include("students.grades.type")
                    .Include("students.grades.subject")
                    .Include("students.grades.subject.subjectGroup")
                    .Include("subjects")
                    .Include("subjects.grades")
                    .Include("subjects.subjectGroup")
                    .First();
                ctx.Dispose();

                if (promotion != null)
                {
                    return promotion;
                }
                return null;
            }
        }

        public static List<Promotion> getActivePromotions()
        {
            using (var ctx = new GestStudContext())
            {
                List<Promotion> list = ctx.Promotions
                    .Include("students")
                    .Include("students.grades")
                    .Include("students.grades.type")
                    .Include("students.grades.subject")
                    .Include("students.grades.subject.subjectGroup")
                    .Include("subjects")
                    .Include("subjects.grades")
                    .Include("subjects.subjectGroup")
                    .Where(p => p.is_active == true)
                    .ToList();

                ctx.Dispose();

                return list;

            }
        }

        public static List<Promotion> getAll()
        {
            using (var ctx = new GestStudContext())
            {
                List<Promotion> list = ctx.Promotions.Include("students")
                    .Include("students.grades")
                    .Include("students.grades.type")
                    .Include("students.grades.subject")
                    .Include("students.grades.subject.subjectGroup")
                    .Include("subjects")
                    .Include("subjects.grades")
                    .Include("subjects.subjectGroup")
                    .ToList();

                ctx.Dispose();

                return list;
            }
        }

        public static Promotion create(Promotion promotion)
        {
            using (var ctx = new GestStudContext())
            {
                promotion.created_at = promotion.updated_at = DateTime.Now;

                ctx.Promotions.Add(promotion);
                ctx.SaveChanges();
                ctx.Dispose();

                return promotion;
            }
        }

        public static void addSubjectsInPromotion(Promotion promotion)
        {
            using (var ctx = new GestStudContext())
            {
                Promotion promotionUpdated = ctx.Promotions
                    .Where(p => p.id == promotion.id)
                    .First();

                ICollection<Subject> subjects = new List<Subject>();

                foreach(Subject s in promotion.subjects)
                {
                    Subject subject = ctx.Subjects.Where(u => u.id == s.id).First();

                    promotionUpdated.subjects.Add(subject);
                }

                ctx.SaveChanges();
                ctx.Dispose();
            }
        }

        public static Promotion removeSubjectsInPromotion(Promotion promotion, Subject subject)
        {
            using (var ctx = new GestStudContext())
            {
                Promotion promotionUpdated = ctx.Promotions
                    .Include("students")
                    .Include("students.grades")
                    .Include("students.grades.type")
                    .Include("students.grades.subject")
                    .Include("students.grades.subject.subjectGroup")
                    .Include("subjects")
                    .Include("subjects.grades")
                    .Include("subjects.subjectGroup")
                    .Where(p => p.id == promotion.id)
                    .First();

                Subject subjectUpdated = ctx.Subjects.Where(u => u.id == subject.id).First();

                promotionUpdated.subjects.Remove(subjectUpdated);

                ctx.SaveChanges();
                ctx.Dispose();

                return promotionUpdated;
            }
        }

        public static Promotion update(Promotion promotion)
        {
            using (var ctx = new GestStudContext())
            {
                Promotion promotionUpdated = ctx.Promotions.Where(p => p.id == promotion.id).First();

                promotionUpdated.name = promotion.name;
                promotionUpdated.years = promotion.years;
                promotionUpdated.is_active = promotion.is_active;
                promotionUpdated.updated_at = DateTime.Now;

                ctx.SaveChanges();
                ctx.Dispose();

                return promotion;
            }
        }

        public static bool delete(Promotion promotion)
        {
            using (var ctx = new GestStudContext())
            {
                Promotion promotionDeleted = ctx.Promotions.Where(u => u.id == promotion.id).First();

                ctx.Promotions.Remove(promotionDeleted);
                ctx.SaveChanges();
                ctx.Dispose();

                return true;
            }
        }
    }
}
