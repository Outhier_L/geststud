﻿using GestStud.Entities;
using GestStud.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestStud.Dao
{
    public static class SubjectDao
    {
        public static Subject getById(int id)
        {
            using (var ctx = new GestStudContext())
            {
                Subject subject = ctx.Subjects
                  .Include("grades")
                  .Include("grades.student")
                  .Include("grades.type")
                  .Where(u => u.id == id).First();
                ctx.Dispose();

                if (subject != null)
                {
                    return subject;
                }
                return null;
            }
        }

        public static Subject getByName(string name)
        {
            using (var ctx = new GestStudContext())
            {
                Subject subject = ctx.Subjects
                  .Include("subjectGroup")
                  .Where(u => u.name == name).First();
                ctx.Dispose();

                if (subject != null)
                {
                    return subject;
                }
                return null;
            }
        }

        public static List<Subject> getAll()
        {
            using (var ctx = new GestStudContext())
            {
                List<Subject> stus = ctx.Subjects.Include("SubjectGroup").Include("Promotions").ToList();
                
                ctx.Dispose();

                return stus;
            }
        }

        public static List<Subject> getDifferenceBetweenListSubjects(List<Subject> list)
        {
            using (var ctx = new GestStudContext())
            {
                List<Subject> stus = ctx.Subjects.Include("SubjectGroup").ToList();

                ctx.Dispose();

                return stus.Where(n => !list.Any(o => n.id == o.id)).ToList();
            }
        }

        public static Subject create(Subject subject)
        {
            using (var ctx = new GestStudContext())
            {
                if(subject.subjectGroup != null)
                {
                    SubjectGroup sg = ctx.SubjectGroups.Where(p => p.id == subject.subjectGroup.id).First();
                    subject.subjectGroup = sg;
                }
                subject.created_at = subject.updated_at = DateTime.Now; 

                ctx.Subjects.Add(subject);
                ctx.SaveChanges();
                ctx.Dispose();

                return subject;
            }
        }

        public static Subject update(Subject subject)
        {
            using (var ctx = new GestStudContext())
            {
                Subject subjectUpdated = ctx.Subjects.Where(u => u.id == subject.id).First();

                if(subject.subjectGroup != null)
                {
                    SubjectGroup subjectGroup = ctx.SubjectGroups.Where(p => p.id == subject.subjectGroup.id).First();
                    subjectUpdated.subjectGroup = subjectGroup;
                }
                
                subjectUpdated.name = subject.name;
                subjectUpdated.coefficient = subject.coefficient;
                subjectUpdated.info_grade = subject.info_grade;
                subjectUpdated.updated_at = DateTime.Now;
                subjectUpdated.remarque = subject.remarque;

                ctx.SaveChanges();
                ctx.Dispose();

                return subject;
            }
        }

        public static bool delete(Subject subject)
        {
            using (var ctx = new GestStudContext())
            {
                Subject subjectDeleted = ctx.Subjects.Where(u => u.id == subject.id).First();

                ctx.Subjects.Remove(subjectDeleted);
                ctx.SaveChanges();
                ctx.Dispose();

                return true;
            }
        }
    }
}
